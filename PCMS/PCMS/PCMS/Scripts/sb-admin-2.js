﻿$(function () {

    $('#side-menu').metisMenu();

});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function () {
    $(window).bind("load resize", function () {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = String( window.location);
    //edit
    if (url.indexOf("create") > 0)
        url = url.substring(0, url.indexOf("create") - 1);
    if (url.indexOf("Create") > 0)
        url = url.substring(0, url.indexOf("Create") - 1);
    if (url.indexOf("edit") > 0)
        url = url.substring(0, url.indexOf("edit") - 1);
    if (url.indexOf("Edit") > 0)
        url = url.substring(0, url.indexOf("Edit") - 1);
    if(url.indexOf("Index") > 0)
        url = url.substring(0, url.indexOf("Index") - 1);
    if (url.indexOf("index") > 0)
        url = url.substring(0, url.indexOf("index") - 1);

    var element = $('ul.nav a').filter(function () {
        if (this.href.split('/').length == 4) {
            var link = this.href.split('/');
            if (this.href.split('/')[3] == "#") {
                return false;
            }
            else if (this.href.split('/')[3] == "") {
                return false;
            }
        }
        return this.href == url ;
    }).addClass('active').parent().parent().addClass('in').parent();
    if (element.is('li')) {
        element.addClass('active');
        element.parent().addClass("in");
        if(element.parent().parent().is('li')){
            element.parent().parent().addClass('active');
        }
    }
});