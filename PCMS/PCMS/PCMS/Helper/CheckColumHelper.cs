﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PCMS.Helper
{
    public class CheckColumHelper
    {
        public static void startCheck()
        {
            //2016_03_23
            //if (!isValidField("md_sieve_RuleGradingCurve", "A16"))
            //{
            //    string query = "ALTER TABLE md_sieve_RuleGradingCurve ADD A16 decimal(16,4) NULL, B16 decimal(16,4) NULL,C16 decimal(16,4) NULL,U16 decimal(16,4) NULL";
            //    var context = new Areas.LIMS.Models.LimsContext();
            //    SqlConnection a = new SqlConnection(context.Database.Connection.ConnectionString);
            //    SqlCommand cmd = a.CreateCommand();
            //    cmd.CommandText = query;
            //    a.Open();
            //    cmd.ExecuteScalar();
            //    a.Close();
            //    a.Dispose();
            //}
            ////2016_04_13 Manuel Schall start            
            //checkAdd("md_material_ConcreteFamilyGroup", "ReferenceSort", "bigint", "NULL");
            //checkAdd("md_material_AggregateDetails", "RuleGradingCurveId", "bigint", "NULL");
            ////2016_04_13 Manuel Schall end
            ////2016_05_11 Manuel Schall start
            //checkAdd("md_Customer", "Discount", "decimal(16,4)", "NULL");
            ////2016_05_11 Manuel Schall end
            ////2016_05_13 Manuel Schall start
            //checkAdd("md_order_LoadingOrder", "UserName", "nvarchar(50)", "NULL");
            ////2016_05_13 Manuel Schall end
            ////2016_05_19 Manuel Schall start
            //checkAdd("md_recipe_Recipe", "GradingCurvSieveRangeId", "bigint", "NULL");
            ////2016_05_19 Manuel Schall end
            ////2016_05_20 Manuel Schall start
            //checkAdd("md_order_LoadingOrder", "Printed", "bit", "NULL");
            ////2016_05_20 Manuel Schall end
            //2016_06_16 Manuel Schall start
            checkAdd("md_order_Weighing", "SealNumber", "nvarchar(MAX)", "NULL");
            //2016_06_16 Manuel Schall end
            //2016_06_17 Manuel Schall start
            checkAdd("md_order_Weighing", "FuelTank", "decimal(18,2)", "NULL");
            //2016_06_17 Manuel Schall end
            //2016_06_17 Manuel Schall start
            checkAdd("md_order_Weighing", "ContainerNo", "nvarchar(100)", "NULL");
            //2016_06_17 Manuel Schall end
            //2016_04_07 Manuel Schall start
            checkAdd("md_Customer", "IsProducer", "bit", "NULL");
            checkAdd("md_Customer", "IsCarrier", "bit", "NULL");
            checkAdd("md_Customer", "IsDisposal", "bit", "NULL");

            checkAdd("md_Customer", "ProducerNumber", "nvarchar(50)", "NULL");
            checkAdd("md_Customer", "CarrierNumber", "nvarchar(50)", "NULL");
            checkAdd("md_Customer", "DisposalNumber", "nvarchar(50)", "NULL");

            checkAdd("md_order_Weighing", "AVVDescription", "nvarchar(MAX)", "NULL");
            checkAdd("md_order_Weighing", "AVVNumber", "nvarchar(50)", "NULL");
            checkAdd("md_order_Weighing", "DisposalProofNumber", "nvarchar(50)", "NULL");

            checkAdd("md_order_Weighing", "ProducerId", "bigint", "NULL");
            checkAdd("md_order_Weighing", "ProducerDescription", "nvarchar(MAX)", "NULL");
            checkAdd("md_order_Weighing", "ProducerStreet", "nvarchar(MAX)", "NULL");
            checkAdd("md_order_Weighing", "ProducerCity", "nvarchar(MAX)", "NULL");
            checkAdd("md_order_Weighing", "ProducerZIPCode", "nvarchar(MAX)", "NULL");
            checkAdd("md_order_Weighing", "ProducerDateTime", "datetime", "NULL");
            checkAdd("md_order_Weighing", "ProducerNumber", "nvarchar(MAX)", "NULL");

            checkAdd("md_order_Weighing", "CarrierId", "bigint", "NULL");
            checkAdd("md_order_Weighing", "CarrierDescription", "nvarchar(MAX)", "NULL");
            checkAdd("md_order_Weighing", "CarrierStreet", "nvarchar(MAX)", "NULL");
            checkAdd("md_order_Weighing", "CarrierCity", "nvarchar(MAX)", "NULL");
            checkAdd("md_order_Weighing", "CarrierZIPCode", "nvarchar(MAX)", "NULL");
            checkAdd("md_order_Weighing", "CarrierDateTime", "datetime", "NULL");
            checkAdd("md_order_Weighing", "CarrierNumber", "nvarchar(MAX)", "NULL");

            checkAdd("md_order_Weighing", "DisposalId", "bigint", "NULL");
            checkAdd("md_order_Weighing", "DisposalDescription", "nvarchar(MAX)", "NULL");
            checkAdd("md_order_Weighing", "DisposalStreet", "nvarchar(MAX)", "NULL");
            checkAdd("md_order_Weighing", "DisposalCity", "nvarchar(MAX)", "NULL");
            checkAdd("md_order_Weighing", "DisposalZIPCode", "nvarchar(MAX)", "NULL");
            checkAdd("md_order_Weighing", "DisposalDateTime", "datetime", "NULL");
            checkAdd("md_order_Weighing", "DisposalNumber", "nvarchar(MAX)", "NULL");

            checkAdd("md_order_Weighing", "UpContainer", "nvarchar(50)", "NULL");
            checkAdd("md_order_Weighing", "PickUpConatiner", "nvarchar(50)", "NULL");

            checkAdd("md_order_Weighing", "EvenPickingUp", "bit", "NULL");
            checkAdd("md_order_Weighing", "EvenDelivery", "bit", "NULL");

            checkAdd("md_order_Weighing", "VehicleNumber", "nvarchar(50)", "NULL");

            checkAdd("md_order_Weighing", "Signature1Id", "bigint", "NULL");
            checkAdd("md_order_Weighing", "Signature2Id", "bigint", "NULL");
            checkAdd("md_order_Weighing", "Signature3Id", "bigint", "NULL");

            checkAdd("md_Config", "Copys", "int", "NULL");



            //2016_04_07 Manuel Schall end


        }
        public static void checkAdd(string tableName, string columnName, string type, string _null)
        {
            if (!isValidField(tableName, columnName))
            {
                addColumn(tableName, columnName, type, _null);
            }
        }
        public static void addColumn(string tableName, string columnName,string type,string _null)
        {
            string query = "ALTER TABLE " +tableName+ " ADD "+ columnName +" "+ type +" "+_null;
            var context = new Areas.LIMS.Models.LimsContext();
            SqlConnection a = new SqlConnection(context.Database.Connection.ConnectionString);
            SqlCommand cmd = a.CreateCommand();
            cmd.CommandText = query;
            a.Open();
            cmd.ExecuteScalar();
            a.Close();
            a.Dispose();
        }
        public static bool isValidField(string tableName, string columnName)
        {
            var tblQuery = "SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS"
                           + " WHERE TABLE_NAME = @tableName AND"
                           + " COLUMN_NAME = @columnName";
            var context = new Areas.LIMS.Models.LimsContext();
            SqlConnection a = new SqlConnection(context.Database.Connection.ConnectionString);
            a.Open();
            SqlCommand cmd = a.CreateCommand();
            cmd.CommandText = tblQuery;
            var tblNameParam = new SqlParameter(
                "@tableName",
                System.Data.SqlDbType.NVarChar,
                128);

            tblNameParam.Value = tableName;
            cmd.Parameters.Add(tblNameParam);
            var colNameParam = new SqlParameter(
                "@columnName",
                System.Data.SqlDbType.NVarChar,
                128);

            colNameParam.Value = columnName;
            cmd.Parameters.Add(colNameParam);
            object objvalid = cmd.ExecuteScalar(); // will return 1 or null
            a.Close();
            a.Dispose();
            return objvalid != null;
        }
    }
    
}