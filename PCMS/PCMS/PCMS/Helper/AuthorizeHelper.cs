﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PCMS.Helper
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class AuthorizeHelper : AuthorizeAttribute
    {
        private string[] UserProfilesRequired { get; set; }
        private Models.MenuContext dbMenu = new Models.MenuContext();
       // private Models.menu_Elements menuElement;

        public AuthorizeHelper(params object[] userProfilesRequired)
        {

            

            // if (userProfilesRequired.Any(p => p.GetType().BaseType != typeof(Enum)))
            //     throw new ArgumentException("userProfilesRequired");
            //
            // this.UserProfilesRequired = userProfilesRequired.Select(p => Enum.GetName(p.GetType(), p)).ToArray();
        }

        public override void OnAuthorization(AuthorizationContext context)
        {
            bool authorized = false;
            var rd = context.RequestContext.RouteData;
            string currentAction = rd.GetRequiredString("action");
            string currentController = rd.GetRequiredString("controller");
            string currentArea = rd.DataTokens["area"] as string;

            Models.menu_Elements menuElement = dbMenu.Menu_Elements.Where(m => m.Area == currentArea && m.Controller == currentController && m.Visible == true).FirstOrDefault();

            if (menuElement == null || menuElement.RequiredRole.Length == 0)
            {
                return ;
            }

            foreach (var role in menuElement.RequiredRole.Split(','))
                if (HttpContext.Current.User.IsInRole(role))
                {
                    authorized = true;
                    break;
                }

            if (!authorized)
            {
                var url = new UrlHelper(context.RequestContext);
                var logonUrl = url.Action("Login", "Account", new { Id = 401, Area = "" });
                context.Result = new RedirectResult(logonUrl);

                return;
            }
        }
    }
}