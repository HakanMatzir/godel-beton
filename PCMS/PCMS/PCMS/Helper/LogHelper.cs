﻿using PCMS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PCMS.Helper
{
    public class LogHelper
    {
    }
    public class LogActionFilter : ActionFilterAttribute

    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            
            Log("OnActionExecuting", filterContext.RouteData, filterContext.HttpContext.Request.UserAgent, filterContext.HttpContext.User.Identity.Name, filterContext.HttpContext.Request.UserHostAddress);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            //Log("OnActionExecuted", filterContext.RouteData, filterContext.HttpContext.Request.UserAgent, filterContext.HttpContext.User.Identity.Name, filterContext.HttpContext.Request.UserHostAddress);
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            //Log("OnResultExecuting", filterContext.RouteData, filterContext.HttpContext.Request.UserAgent, filterContext.HttpContext.User.Identity.Name, filterContext.HttpContext.Request.UserHostAddress);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            //Log("OnResultExecuted", filterContext.RouteData,filterContext.HttpContext.Request.UserAgent, filterContext.HttpContext.User.Identity.Name, filterContext.HttpContext.Request.UserHostAddress);
        }


        private void Log(string methodName, RouteData routeData,string userAgent,string user, string ip)
        {


            LogContext db = new LogContext();
            var controllerName = routeData.Values["controller"];
            var actionName = routeData.Values["action"];

            LogWrite l = new LogWrite();
            l.Timestamp = DateTime.Now;
            l.Controller = controllerName.ToString();
            l.Action = actionName.ToString();
            l.UserAgent = userAgent;
            l.UserName = user;
            l.IP = ip;
            
            db.LogWrite.Add(l);
            //db.SaveChanges();
            db.SaveChangesAsync();

            //var message = String.Format("{0} controller:{1} action:{2} user:{3}", methodName, controllerName, actionName,user);
            //Debug.WriteLine(message, "Action Filter Log");
        }

    }
    public static class AuditLogH
    {
        public static List<Areas.LIMS.Models.AuditLog> GetAuditRecordsForChange(DbEntityEntry dbEntry, string userId)
        {
            List<Areas.LIMS.Models.AuditLog> result = new List<Areas.LIMS.Models.AuditLog>();

            DateTime changeTime = DateTime.Now;

            // Get the Table() attribute, if one exists
            //TableAttribute tableAttr = dbEntry.Entity.GetType().GetCustomAttributes(typeof(TableAttribute), false).SingleOrDefault() as TableAttribute;

            TableAttribute tableAttr = dbEntry.Entity.GetType().GetCustomAttributes(typeof(TableAttribute), true).SingleOrDefault() as TableAttribute;

            // Get table name (if it has a Table attribute, use that, otherwise get the pluralized name)
            string tableName = tableAttr != null ? tableAttr.Name : dbEntry.Entity.GetType().Name;

            // Get primary key value (If you have more than one key column, this will need to be adjusted)
            //var keyNames = dbEntry.Entity.GetType().GetProperties().Where(p => p.GetCustomAttributes(typeof(KeyAttribute), false).Count() > 0).ToList();
            var keyNames = dbEntry.Entity.GetType().GetProperties().ToList();
            string keyName = keyNames[0].Name; //dbEntry.Entity.GetType().GetProperties().Single(p => p.GetCustomAttributes(typeof(KeyAttribute), false).Count() > 0).Name;
            //keyName = "Id";
            //string keyName = dbEntry.Entity.GetType().GetProperties().Single(p => p.GetCustomAttributes(typeof(KeyAttribute), false).Count() > 0).Name;

            if (dbEntry.State == EntityState.Added)
            {
                // For Inserts, just add the whole record
                // If the entity implements IDescribableEntity, use the description from Describe(), otherwise use ToString()
                try
                {
                    foreach (string propertyName in dbEntry.CurrentValues.PropertyNames)
                    {
                        //var org = dbEntry.GetDatabaseValues().GetValue<object>(propertyName);
                        result.Add(new Areas.LIMS.Models.AuditLog()
                        {
                            auditlogid = Guid.NewGuid(),
                            userid = userId,
                            eventdateutc = changeTime,
                            eventtype = "A",    // Added
                            tablename = tableName,
                            recordid = dbEntry.CurrentValues.GetValue<object>(keyName).ToString(),
                            columnname = propertyName,
                            //originalvalue = org.ToString() ?? null,
                            newvalue = dbEntry.CurrentValues.GetValue<object>(propertyName) == null ? null : dbEntry.CurrentValues.GetValue<object>(propertyName).ToString()
                        }
                                );
                    }
                }
                catch (Exception e)
                {
                    Helper.ExceptionHelper.LogException(e, userId);
                    Console.Write(e);
                }
            }
            else if (dbEntry.State == EntityState.Deleted)
            {
                try
                {


                    // Same with deletes, do the whole record, and use either the description from Describe() or ToString()
                    result.Add(new Areas.LIMS.Models.AuditLog()
                    {
                        auditlogid = Guid.NewGuid(),
                        userid = userId,
                        eventdateutc = changeTime,
                        eventtype = "D", // Deleted
                        tablename = tableName,
                        recordid = dbEntry.OriginalValues.GetValue<object>(keyName).ToString(),
                        columnname = "*ALL",
                        //newvalue = (dbEntry.OriginalValues.ToObject() is IDescribableEntity) ? (dbEntry.OriginalValues.ToObject() as IDescribableEntity).Describe() : dbEntry.OriginalValues.ToObject().ToString()
                        newvalue = dbEntry.OriginalValues.ToObject().ToString()
                    }
                        );
                }
                catch (Exception e)
                {
                    Helper.ExceptionHelper.LogException(e, userId);
                    Console.Write(e);
                }
            }
            else if (dbEntry.State == EntityState.Modified)
            {
                try {
                    foreach (string propertyName in dbEntry.OriginalValues.PropertyNames)
                    {
                        // For updates, we only want to capture the columns that actually changed
                        var org = dbEntry.GetDatabaseValues().GetValue<object>(propertyName);
                        var curr = dbEntry.CurrentValues.GetValue<object>(propertyName);

                        if (!object.Equals(org, curr))
                        {
                            result.Add(new Areas.LIMS.Models.AuditLog()
                            {
                                auditlogid = Guid.NewGuid(),
                                userid = userId,
                                eventdateutc = changeTime,
                                eventtype = "M",    // Modified
                                tablename = tableName,
                                recordid = dbEntry.OriginalValues.GetValue<object>(keyName).ToString(),
                                columnname = propertyName,
                                originalvalue = org == null ? null : org.ToString(),
                                newvalue = dbEntry.CurrentValues.GetValue<object>(propertyName) == null ? null : dbEntry.CurrentValues.GetValue<object>(propertyName).ToString()
                            }
                                );
                        }
                    }
                }
                catch (Exception e)
            {
                    Helper.ExceptionHelper.LogException(e, userId);
                    Console.Write(e);
                }
        }
            // Otherwise, don't do anything, we don't care about Unchanged or Detached entities

            return result;
        }
    }
}