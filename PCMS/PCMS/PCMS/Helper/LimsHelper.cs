﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace PCMS.Helper
{
    public class LimsHelper
    {
        static public void limsLoop()
        {
            Areas.MasterData.Models.MasterDataContext dbMaster = new Areas.MasterData.Models.MasterDataContext();
            Areas.Order.Models.OrderContext dbLoading = new Areas.Order.Models.OrderContext();
            Areas.LIMS.Models.LimsContext dbLims = new Areas.LIMS.Models.LimsContext();
            var concreteFamilies = dbMaster.Md_masterData_ConcreteFamily.ToList();
            foreach (var conFamily in concreteFamilies)
            {
                var sorts = dbMaster.Md_material_SortDetails.Where(a => a.ConcreteFamilyId == conFamily.Id).Select(a => a.Id).ToList();
                var artikel = dbMaster.Md_masterData_Material.Where(a => sorts.Contains(a.SortId)).Select(a => a.Id).ToList();
                int maxProbs = 0;
                if (conFamily.InitialProduction_0_Count > maxProbs)
                    maxProbs = conFamily.InitialProduction_0_Count;
                if (conFamily.InitialProduction_1_Count > maxProbs)
                    maxProbs = conFamily.InitialProduction_1_Count;
                if (conFamily.SteadyProduction_0_Count > maxProbs)
                    maxProbs = conFamily.SteadyProduction_0_Count;
                if (conFamily.SteadyProduction_1_Count > maxProbs)
                    maxProbs = conFamily.SteadyProduction_1_Count;
                var lastTests = dbLims.Lims_concrete_Test.Where(a => (a.Stauts == 2 || a.Stauts == 3) && artikel.Contains(a.SortId) && a.IsDeleted == false).OrderByDescending(a => a.TestDate).Select(a=>a.TestDate).Take(maxProbs).ToList();
                var lastTest = dbLims.Lims_concrete_Test.Where(a => (a.Stauts == 2 || a.Stauts == 3) && artikel.Contains(a.SortId) && a.IsDeleted == false).OrderByDescending(a => a.TestDate).FirstOrDefault();
                DateTime dateTest = lastTest != null ? lastTests.Last().Date : DateTime.Now;
                var loadinOrders = (lastTest != null ? dbLoading.Md_order_LoadingOrder.Where(a => a.RegistrationDate >= dateTest && artikel.Contains(a.MaterialId) && a.State==4 ).OrderBy(a => a.RegistrationDate).ToList() : dbLoading.Md_order_LoadingOrder.Where(a => artikel.Contains(a.MaterialId)).OrderBy(a => a.RegistrationDate).ToList());
                if (loadinOrders.Count != 0 && lastTests.Count !=0)
                {
                    //Stetige Herstellung
                    if (conFamily.InitialProduction == false)
                    {
                        //Menge
                        var test = loadinOrders.Sum(a => a.OrderedQuantity);
                        //test = loadinOrders.Where(a => a.RegistrationDate >= lastTests.ElementAt(conFamily.SteadyProduction_0_Count - 1)).Sum(a => a.OrderedQuantity);
                        if (lastTests.Count < conFamily.SteadyProduction_0_Count ||  loadinOrders.Where(a=>a.RegistrationDate >= lastTests.ElementAt(conFamily.SteadyProduction_0_Count - 1)).Sum(a => a.OrderedQuantity) >= conFamily.SteadyProduction_0_val)
                        {
                            dbMaster.Entry(conFamily).State = System.Data.Entity.EntityState.Modified;
                            conFamily.AmountTestRequired = true;
                        }
                        else
                        {
                            dbMaster.Entry(conFamily).State = System.Data.Entity.EntityState.Modified;
                            conFamily.AmountTestRequired = false;
                        }
                        //Tage
                        if ((lastTests.Count < conFamily.SteadyProduction_1_Count || loadinOrders.Where(a => a.RegistrationDate >= lastTests.ElementAt(conFamily.SteadyProduction_1_Count - 1)).GroupBy(a => a.RegistrationDate.Value.Date).Count() >= conFamily.SteadyProduction_1_Val))
                        {
                            dbMaster.Entry(conFamily).State = System.Data.Entity.EntityState.Modified;
                            conFamily.TimeTestRequired = true;
                        }
                        else
                        {
                            dbMaster.Entry(conFamily).State = System.Data.Entity.EntityState.Modified;
                            conFamily.TimeTestRequired = false;
                        }
                        dbMaster.SaveChanges();
                    }
                    //Erstherstellung
                    else
                    {
                        //Menge
                        var test = loadinOrders.Sum(a => a.OrderedQuantity);
                            //test = loadinOrders.Where(a => a.RegistrationDate >= lastTests.ElementAt(conFamily.InitialProduction_0_Count - 1)).Sum(a => a.OrderedQuantity);
                        if (lastTests.Count < conFamily.InitialProduction_0_Count || loadinOrders.Where(a => a.RegistrationDate >= lastTests.ElementAt(conFamily.InitialProduction_0_Count - 1)).Sum(a => a.OrderedQuantity) >= conFamily.InitialProduction_0_Val)
                        {
                            dbMaster.Entry(conFamily).State = System.Data.Entity.EntityState.Modified;
                            conFamily.AmountTestRequired = true;
                            
                        }
                        else
                        {
                            dbMaster.Entry(conFamily).State = System.Data.Entity.EntityState.Modified;
                            conFamily.AmountTestRequired = false;
                        }
                        //Tage
                        test = loadinOrders.Where(a => a.RegistrationDate >= lastTests.ElementAt(conFamily.InitialProduction_1_Count - 1)).GroupBy(a => a.RegistrationDate.Value.Date).Count();
                        if (lastTests.Count < conFamily.InitialProduction_1_Count || loadinOrders.Where(a => a.RegistrationDate >= lastTests.ElementAt(conFamily.InitialProduction_1_Count - 1)).GroupBy(a => a.RegistrationDate.Value.Date).Count() >= conFamily.InitialProduction_1_Val)
                        {
                            dbMaster.Entry(conFamily).State = System.Data.Entity.EntityState.Modified;
                            conFamily.TimeTestRequired = true;
                        }
                        else
                        {
                            dbMaster.Entry(conFamily).State = System.Data.Entity.EntityState.Modified;
                            conFamily.TimeTestRequired = false;
                        }
                        dbMaster.SaveChanges();


                    }

                }
                else
                {
                    dbMaster.Entry(conFamily).State = System.Data.Entity.EntityState.Modified;
                    conFamily.AmountTestRequired = true;
                    conFamily.TimeTestRequired = true;
                    dbMaster.SaveChanges();
                }


            }
        }
    }

}