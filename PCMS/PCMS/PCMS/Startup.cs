﻿using Microsoft.Owin;
using Owin;
using System.Data.Entity;
using System.Threading;
using System.Web.Security;

[assembly: OwinStartupAttribute(typeof(PCMS.Startup))]
namespace PCMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {


            Database.SetInitializer<Areas.Weigh.Models.WeighContext>(new CreateDatabaseIfNotExists<Areas.Weigh.Models.WeighContext>());

            ConfigureAuth(app);
            //Roles.CreateRole("admin");
            Helper.CheckColumHelper.startCheck();//check colum
            //if(Helper.Definitions.getFactoryGroup() == 0)
            //{               
            //    Thread deleiveryNoteThread = new Thread(Helper.DeliveryNotePrint.DeliveryNotePrintLoop);
            //    deleiveryNoteThread.Start();                
            //}
            
        }
    }
}
