﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.Comment.Models;
using PCMS.Models;
using PagedList;
using Newtonsoft.Json;
using Microsoft.Reporting.WebForms;

namespace PCMS.Areas.Comment.Controllers
{
    public class CommentsController : Controller
    {
        private CommentContext db = new CommentContext();

        // GET: Comment/Comments
        public ActionResult Index(PaginationModel pg)
        {
            var model = db.Comment.OrderBy(a => a.CreateDate).ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Title":
                                model = model.Where(m => m.Title != null && m.Title.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Titlel = pgFF.colVal;
                                break;
                            case "Status":
                                model = model.Where(m =>  m.Status.Equals(Convert.ToInt32(pgFF.colVal))).ToList();
                                ViewBag.Status = pgFF.colVal;
                                break;
                            case "Place":
                                model = model.Where(m =>  m.Place.Equals(pgFF.colVal)).ToList();
                                ViewBag.Place = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "CreateDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.CreateDate).ToList();
                    else
                        model = model.OrderByDescending(m => m.CreateDate).ToList();
                    break;
                case "Title":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Title).ToList();
                    else
                        model = model.OrderByDescending(m => m.Title).ToList();
                    break;
                case "Comment":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Comment1).ToList();
                    else
                        model = model.OrderByDescending(m => m.Comment1).ToList();
                    break;
                case "Status":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Status).ToList();
                    else
                        model = model.OrderByDescending(m => m.Status).ToList();
                    break;
                case "Place":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Place).ToList();
                    else
                        model = model.OrderByDescending(m => m.Place).ToList();
                    break;
                case "UserName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.UserName).ToList();
                    else
                        model = model.OrderByDescending(m => m.UserName).ToList();
                    break;
                case "Comment2":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Comment2).ToList();
                    else
                        model = model.OrderByDescending(m => m.Comment2).ToList();
                    break;
                case "UserName2":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.UserName2).ToList();
                    else
                        model = model.OrderByDescending(m => m.UserName2).ToList();
                    break;
            }
            ViewBag.Status = new SelectList(db.CommentStatus, "Id", "Description", ViewBag.Status);

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }


        // GET: Comment/Comments/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CommentC comment = db.Comment.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        // GET: Comment/Comments/Create
        public ActionResult Create()
        {
            PCMS.Models.MenuContext db2 = new MenuContext();
            ViewBag.Place = new SelectList(db2.Menu_Groups.Where(a=>a.Visible==true), "Id", "Label");
            ViewBag.Status = new SelectList(db.CommentStatus, "Id", "Description");
            return View();
        }

        // POST: Comment/Comments/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Comment1,Place")] CommentC comment)
        {
            if (ModelState.IsValid)
            {

                comment.CreateDate = DateTime.Now;
                comment.UserName = User.Identity.Name;
                comment.Status = 1;
                comment.Comment2 = null;
                comment.UserName2 = null;
                db.Comment.Add(comment);
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }
            PCMS.Models.MenuContext db2 = new MenuContext();
            ViewBag.Place = new SelectList(db2.Menu_Groups.Where(a => a.Visible == true), "Id", "Label",comment.Place);
            ViewBag.Status = new SelectList(db.CommentStatus, "Id", "Description",comment.Status);
            return View(comment);
        }

        // GET: Comment/Comments/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CommentC comment = db.Comment.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            PCMS.Models.MenuContext db2 = new MenuContext();
            ViewBag.Place = new SelectList(db2.Menu_Groups.Where(a => a.Visible == true), "Id", "Label",comment.Place);
            ViewBag.Status = new SelectList(db.CommentStatus, "Id", "Description",comment.Status);
            return View(comment);
        }

        // POST: Comment/Comments/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,CreateDate,Title,Comment1,Status,Place,UserName,Comment2,UserName2")] CommentC comment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(comment).State = EntityState.Modified;
                db.SaveChanges(User.Identity.Name);
                return RedirectToAction("Index");
            }
            PCMS.Models.MenuContext db2 = new MenuContext();
            ViewBag.Place = new SelectList(db2.Menu_Groups.Where(a => a.Visible == true), "Id", "Label",comment.Place);
            ViewBag.Status = new SelectList(db.CommentStatus, "Id", "Description",comment.Status);
            return View(comment);
        }

        // GET: Comment/Comments/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CommentC comment = db.Comment.Find(id);
            if (comment == null)
            {
                return HttpNotFound();
            }
            return View(comment);
        }

        // POST: Comment/Comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            CommentC comment = db.Comment.Find(id);
            db.Comment.Remove(comment);
            db.SaveChanges(User.Identity.Name);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult Report(string pgS)
        {
            PCMS.Models.PaginationModel pg = JsonConvert.DeserializeObject<PCMS.Models.PaginationModel>(pgS);
            var model = db.Comment.OrderBy(a => a.CreateDate).ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Title":
                                model = model.Where(m => m.Title != null && m.Title.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Titlel = pgFF.colVal;
                                break;
                            case "Status":
                                model = model.Where(m => m.Status.Equals(Convert.ToInt32(pgFF.colVal))).ToList();
                                ViewBag.Status = pgFF.colVal;
                                break;
                            case "Place":
                                model = model.Where(m => m.Place.Equals(pgFF.colVal)).ToList();
                                ViewBag.Place = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "CreateDate":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.CreateDate).ToList();
                    else
                        model = model.OrderByDescending(m => m.CreateDate).ToList();
                    break;
                case "Title":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Title).ToList();
                    else
                        model = model.OrderByDescending(m => m.Title).ToList();
                    break;
                case "Comment":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Comment1).ToList();
                    else
                        model = model.OrderByDescending(m => m.Comment1).ToList();
                    break;
                case "Status":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Status).ToList();
                    else
                        model = model.OrderByDescending(m => m.Status).ToList();
                    break;
                case "Place":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Place).ToList();
                    else
                        model = model.OrderByDescending(m => m.Place).ToList();
                    break;
                case "UserName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.UserName).ToList();
                    else
                        model = model.OrderByDescending(m => m.UserName).ToList();
                    break;
                case "Comment2":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Comment2).ToList();
                    else
                        model = model.OrderByDescending(m => m.Comment2).ToList();
                    break;
                case "UserName2":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.UserName2).ToList();
                    else
                        model = model.OrderByDescending(m => m.UserName2).ToList();
                    break;
            }

            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();
            foreach (var dataset in model)
            {
                ds.List.AddListRow(dataset.CommentStatus.Description, dataset.menuGroup.Label, dataset.Title, dataset.Comment1 , dataset.Comment2, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            }
            ReportDataSource DSReport = new ReportDataSource("dsList", ds.List.ToList());

            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Header_00", "Status"));
            repParams.Add(new ReportParameter("Hide_00", "false"));
            repParams.Add(new ReportParameter("Header_01", "Bereich"));
            repParams.Add(new ReportParameter("Hide_01", "false"));
            repParams.Add(new ReportParameter("Header_02", "Thema"));
            repParams.Add(new ReportParameter("Hide_02", "false"));
            repParams.Add(new ReportParameter("Header_03", "Kommentar"));
            repParams.Add(new ReportParameter("Hide_03", "false"));
            repParams.Add(new ReportParameter("Header_04", "Antwort"));
            repParams.Add(new ReportParameter("Hide_04", "false"));
            repParams.Add(new ReportParameter("Header_05", "Min"));
            repParams.Add(new ReportParameter("Hide_05", "true"));
            repParams.Add(new ReportParameter("Header_06", "Bestand"));
            repParams.Add(new ReportParameter("Hide_06", "true"));
            repParams.Add(new ReportParameter("Header_07", ""));
            repParams.Add(new ReportParameter("Hide_07", "true"));
            repParams.Add(new ReportParameter("Header_08", ""));
            repParams.Add(new ReportParameter("Hide_08", "true"));
            repParams.Add(new ReportParameter("Header_09", ""));
            repParams.Add(new ReportParameter("Hide_09", "true"));

            repParams.Add(new ReportParameter("Title", "Kommentare"));
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToShortDateString()));
            repParams.Add(new ReportParameter("Description", "Kommentare Übersicht"));
            repParams.Add(new ReportParameter("Footer", "Gesamtzahl Einträge: " + model.Count()));

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/Comment/ListReport.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 800;
            reportViewer.Height = 700;

            //------------------
            string mimeType = "";
            string encoding = "";
            string filenameExtension = "";
            string[] streamids = null;
            Warning[] warnings = null;

            byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
            return File(file, mimeType);
            //ViewBag.ReportViewer = reportViewer;
            //return View();


        }
    }
}
