﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PCMS.Areas.Comment.Models
{
    public class CommentContext : DbContext
    {
        public CommentContext()
            : base("DefaultConnection")
        {
        }

        public virtual DbSet<CommentC> Comment { get; set; }
        public virtual DbSet<CommentStatus> CommentStatus { get; set; }
        public virtual DbSet<LIMS.Models.AuditLog> Auditlog { get; set; }

        public int SaveChanges(string userId)
        {
            // Get all Added/Deleted/Modified entities (not Unmodified or Detached)
            foreach (var ent in this.ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified))
            {
                // For each changed record, get the audit record entries and add them
                foreach (LIMS.Models.AuditLog x in Helper.AuditLogH.GetAuditRecordsForChange(ent, userId))
                {
                    this.Auditlog.Add(x);
                }
            }

            // Call the original SaveChanges(), which will save both the changes made and the audit records
            return base.SaveChanges();
        }

    }
    [Table("Comment")]
    public partial class CommentC
    {
        [Key]
        public long Id { get; set; }
        [DisplayName("ErstellungsDatum")]
        public DateTime CreateDate { get; set; }
        [DisplayName("Thema")]
        public string Title { get; set; }
        [DisplayName("Kommentar")]
        [Column("Comment")]
        public string Comment1 { get; set; }
        [DisplayName("Status")]
        public long Status { get; set; }
        [DisplayName("Bereich")]
        public long Place { get; set; }
        [DisplayName("Ersteller")]
        public string UserName { get; set; }
        [DisplayName("Antwort")]
        public string Comment2 { get; set; }
        [DisplayName("Bearbeiter")]
        public string UserName2 { get; set; }
        [ForeignKey("Status")]
        public virtual CommentStatus CommentStatus { get; set; }
        [ForeignKey("Place")]
        public virtual PCMS.Models.menu_Groups menuGroup { get; set; }
    }
    [Table("CommentStatus")]
    public partial class CommentStatus
    {
        [Key]
        public long Id { get; set; }
        public string Description { get; set; }
    }

    public class CommentModel
    {
    }
}