﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PCMS.Areas.Admin.Controllers
{
    public class PrintController : Controller
    {
        // GET: Admin/Print
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult setPrinter(string printerName)
        {
            return Json( Helper.Definitions.setPriner(printerName));
        }


    }
}