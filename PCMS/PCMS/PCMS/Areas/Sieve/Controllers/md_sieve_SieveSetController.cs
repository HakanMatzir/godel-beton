﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.Sieve.Models;
using System.Globalization;
using PagedList;
using Microsoft.Reporting.WebForms;
using PCMS.Helper;

namespace PCMS.Areas.Sieve.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_sieve_SieveSetController : Controller
    {
        private SieveContext db = new SieveContext();

        // GET: Sieve/md_sieve_SieveSet
        public ActionResult Index(PCMS.Models.PaginationModel pg)
        {
            var model = db.Md_sieve_SieveSet.OrderBy(a => a.Description).ToList();
            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Description":
                                model = model.Where(m => m.Description != null && m.Description.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Description = pgFF.colVal;
                                break;
                            case "Number":
                                model = model.Where(m => m.Number != null && m.Number.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Number = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Description":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Description).ToList();
                    else
                        model = model.OrderByDescending(m => m.Description).ToList();
                    break;
                case "Number":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Number).ToList();
                    else
                        model = model.OrderByDescending(m => m.Number).ToList();
                    break;
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }


        // GET: Sieve/md_sieve_SieveSet/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_sieve_SieveSet md_sieve_SieveSet = db.Md_sieve_SieveSet.Find(id);
            if (md_sieve_SieveSet == null)
            {
                return HttpNotFound();
            }
            return View(md_sieve_SieveSet);
        }

        // GET: Sieve/md_sieve_SieveSet/Create
        public ActionResult Create()
        {
            
            var sl = db.Md_sieve_Sieves.OrderBy(m => m.Size).Select(m => new SelectListItem { Text = m.Size.ToString(), Value = m.Id.ToString() }).ToList();

            
            foreach (SelectListItem ba in sl)
            {
                ba.Text = getShortString(decimal.Parse(ba.Text, CultureInfo.InvariantCulture));
            }
            sl.Insert(0, new SelectListItem { Text = "---", Value = "-1" });
            //ViewBag.VehicleGroupId = db.Md_sieve_Sieves.OrderBy(m => m.Size).Select(m => new SelectListItem { Text = m.Size.ToString(), Value = m.Id.ToString() }).ToList();
            ViewBag.Sieves = sl;
            //ViewBag.VehicleGroupId = new SelectList(db.Md_sieve_Sieves, "Id", "Size");
            return View();
        }

        // POST: Sieve/md_sieve_SieveSet/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Description,Number")] md_sieve_SieveSet md_sieve_SieveSet, string[] Sieves)
        {

            if (ModelState.IsValid)
            {
                
                db.Md_sieve_SieveSet.Add(md_sieve_SieveSet);
                db.SaveChanges();

                //var item = db.Entry<Models.md_sieve_SieveSet>(md_sieve_SieveSet);
                //item.State = EntityState.Modified;

                //item.Collection(i => i.Sieves).Load();
                //md_sieve_SieveSet.Sieves.Clear();
                if (Sieves != null)
                {
                    foreach (string s in Sieves)
                    {
                        if (s != "-1")
                            md_sieve_SieveSet.Sieves.Add(db.Md_sieve_Sieves.Find(Convert.ToInt64(s)));
                    }
                }
                db.Entry(md_sieve_SieveSet).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(md_sieve_SieveSet);
        }

        // GET: Sieve/md_sieve_SieveSet/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_sieve_SieveSet md_sieve_SieveSet = db.Md_sieve_SieveSet.Find(id);
            if (md_sieve_SieveSet == null)
            {
                return HttpNotFound();
            }
            var sl = db.Md_sieve_Sieves.OrderBy(m => m.Size).Select(m => new SelectListItem { Text = m.Size.ToString(), Value = m.Id.ToString() }).ToList();
            foreach (SelectListItem ba in sl)
            {
                ba.Text = getShortString(decimal.Parse(ba.Text, CultureInfo.InvariantCulture));
            }
            
            ViewBag.Sieves = sl;
            md_sieve_SieveSet.Sieves = md_sieve_SieveSet.Sieves.OrderBy(s => s.Size).ToList();
            return View(md_sieve_SieveSet);
        }

        // POST: Sieve/md_sieve_SieveSet/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Description,Number")] md_sieve_SieveSet md_sieve_SieveSet, string[] Sieves)
        {
            if (ModelState.IsValid)
            {
                db.Entry(md_sieve_SieveSet).State = EntityState.Modified;


                db.Entry(md_sieve_SieveSet).Collection(i => i.Sieves).Load();
                md_sieve_SieveSet.Sieves.Clear();
                if (Sieves != null)
                {
                    foreach (string s in Sieves)
                    {
                        if(!String.IsNullOrEmpty(s))
                            md_sieve_SieveSet.Sieves.Add(db.Md_sieve_Sieves.Find(Convert.ToInt64(s)));
                    }
                }



                db.SaveChanges();
                db.Database.Log = Console.Write;
                return RedirectToAction("Index");
            }
            return View(md_sieve_SieveSet);
        }

        // GET: Sieve/md_sieve_SieveSet/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_sieve_SieveSet md_sieve_SieveSet = db.Md_sieve_SieveSet.Find(id);
            if (md_sieve_SieveSet == null)
            {
                return HttpNotFound();
            }
            return View(md_sieve_SieveSet);
        }

        // POST: Sieve/md_sieve_SieveSet/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            md_sieve_SieveSet md_sieve_SieveSet = db.Md_sieve_SieveSet.Find(id);
            md_sieve_SieveSet.Sieves.Clear();
            db.Entry(md_sieve_SieveSet).State = EntityState.Modified;
            db.SaveChanges();


            db.Md_sieve_SieveSet.Remove(md_sieve_SieveSet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //***************************************************************************Reporting
        public ActionResult Report(PCMS.Models.PaginationModel pg)
        {
            var model = pageModel(pg);
            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();
            foreach (var dataset in model)
            {
                ds.List.AddListRow(dataset.Id.ToString(), dataset.Description, dataset.AllSives.ToString(), dataset.Number.ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            }
            ReportDataSource DSReport = new ReportDataSource("dsList", ds.List.ToList());

            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Header_00", "ID"));
            repParams.Add(new ReportParameter("Hide_00", "false"));
            repParams.Add(new ReportParameter("Header_01", "Description"));
            repParams.Add(new ReportParameter("Hide_01", "false"));
            repParams.Add(new ReportParameter("Header_02", "Alle Siebe"));
            repParams.Add(new ReportParameter("Hide_02", "false"));
            repParams.Add(new ReportParameter("Header_03", "Nummer"));
            repParams.Add(new ReportParameter("Hide_03", "false"));
            repParams.Add(new ReportParameter("Header_04", ""));
            repParams.Add(new ReportParameter("Hide_04", "true"));
            repParams.Add(new ReportParameter("Header_05", ""));
            repParams.Add(new ReportParameter("Hide_05", "true"));
            repParams.Add(new ReportParameter("Header_06", ""));
            repParams.Add(new ReportParameter("Hide_06", "true"));
            repParams.Add(new ReportParameter("Header_07", ""));
            repParams.Add(new ReportParameter("Hide_07", "true"));
            repParams.Add(new ReportParameter("Header_08", ""));
            repParams.Add(new ReportParameter("Hide_08", "true"));
            repParams.Add(new ReportParameter("Header_09", ""));
            repParams.Add(new ReportParameter("Hide_09", "true"));

            repParams.Add(new ReportParameter("Title", "Siebsätze"));
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToShortDateString()));
            repParams.Add(new ReportParameter("Description", "Liste der Siebsätze"));
            repParams.Add(new ReportParameter("Footer", "Gesamtzahl Einträge: " + model.Count()));

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/ListReport.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 1200;
            reportViewer.Height = 700;

            ViewBag.ReportViewer = reportViewer;

            return PartialView();
        }

        public ActionResult ReportDetail(long Id)
        {
            var model = db.Md_sieve_SieveSet.Where(a => a.Id == Id);

            var Sieves = new List<md_sieve_Sieves>();
            Sieves = model.First().Sieves.OrderBy(s => s.Size).ToList();

            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Title", "Siebsatz"));
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToShortDateString()));
            repParams.Add(new ReportParameter("Description", "Details des Siebesatzes"));
            repParams.Add(new ReportParameter("Footer", "Anzahl Siebe: " + Sieves.Count.ToString()));
            repParams.Add(new ReportParameter("Id", model.First().Id.ToString()));
            repParams.Add(new ReportParameter("Name", model.First().Description.ToString()));
            repParams.Add(new ReportParameter("Number", model.First().Number.ToString()));

            string SievesList = "";
            for(int i = 0; i< Sieves.Count; i++)
            {
                SievesList += Sieves[i].Size.ToString() + ";";
            }
            for (int i = 0; i < (17-Sieves.Count); i++)
            {
                SievesList +=  "-;";
            }

            repParams.Add(new ReportParameter("SievesList", SievesList));
           
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/Sieve/SieveSetDetail.rdlc";
            //reportViewer.ShowPrintButton = false;
            //reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);
            

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 1200;
            reportViewer.Height = 700;

            ViewBag.ReportViewer = reportViewer;

            return PartialView("Report");
        }


        private List<md_sieve_SieveSet> pageModel(PCMS.Models.PaginationModel pg)
        {
            var model = db.Md_sieve_SieveSet.OrderBy(a => a.Description).ToList();
            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Description":
                                model = model.Where(m => m.Description != null && m.Description.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Description = pgFF.colVal;
                                break;
                            case "Number":
                                model = model.Where(m => m.Number != null && m.Number.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Number = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Description":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Description).ToList();
                    else
                        model = model.OrderByDescending(m => m.Description).ToList();
                    break;
                case "Number":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Number).ToList();
                    else
                        model = model.OrderByDescending(m => m.Number).ToList();
                    break;
            }
            return model;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public string getShortString(decimal ta)
        {
            
            return ta.ToString("0.####");
        }
    }
}
