﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.Sieve.Models;
using PagedList;
using PCMS.Helper;
using Microsoft.Reporting.WebForms;


namespace PCMS.Areas.Sieve.Controllers
{
    [LogActionFilter]
    //[Authorize]
    public class md_sieve_RuleGradingCurveController : Controller
    {
        private SieveContext db = new SieveContext();

        // GET: Sieve/md_sieve_RuleGradingCurve
        public ActionResult Index(PCMS.Models.PaginationModel pg,long? id)
        {
            
            var model = db.Md_sieve_RuleGradingCurve.Where(m=>m.IsDeleted == false).Include(m => m.md_sieve_SieveSet).Include(m => m.md_material_GradinGroup).ToList();
            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Number":
                                model = model.Where(m => m.Number != null && m.Number.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Number = pgFF.colVal;
                                break;
                            case "Description":
                                model = model.Where(m => m.Description != null && m.Description.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Description = pgFF.colVal;
                                break;
                            case "SieveSetId":
                                model = model.Where(m => m.SieveSetId.Equals(Convert.ToInt64( pgFF.colVal))).ToList();
                                ViewBag.SieveSetId = Convert.ToInt64(pgFF.colVal);
                                break;
                            case "GradingId":
                                model = model.Where(m => m.GradingId.Equals(Convert.ToInt64( pgFF.colVal))).ToList();
                                ViewBag.GradingId = Convert.ToInt64(pgFF.colVal);
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Number":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Number).ToList();
                    else
                        model = model.OrderByDescending(m => m.Number).ToList();
                    break;
                case "Description":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Description).ToList();
                    else
                        model = model.OrderByDescending(m => m.Description).ToList();
                    break;
                case "SieveSetId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.SieveSetId).ToList();
                    else
                        model = model.OrderByDescending(m => m.SieveSetId).ToList();
                    break;
                case "GradingId":
                    if (pg.orderDir.Equals("desc"))
                        //model = model.OrderBy(m => m.GradingId).ToList();
                        model = model.OrderBy(m => m.md_material_GradinGroup.Description).ToList();
                    else
                        //model = model.OrderByDescending(m => m.GradingId).ToList();
                        model = model.OrderByDescending(m => m.md_material_GradinGroup.Description).ToList();
                    break;
                
            }
            ViewBag.SieveSetId = new SelectList(db.Md_sieve_SieveSet, "Id", "Description",(ViewBag.SieveSetId ?? null));
            ViewBag.GradingId = new SelectList(db.md_material_GradingGroup, "Id", "Description",(ViewBag.GradingId ?? null));
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }


        // GET: Sieve/md_sieve_RuleGradingCurve/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_sieve_RuleGradingCurve md_sieve_RuleGradingCurve = db.Md_sieve_RuleGradingCurve.Find(id);
            if (md_sieve_RuleGradingCurve == null)
            {
                return HttpNotFound();
            }
            return View(md_sieve_RuleGradingCurve);
        }

        // GET: Sieve/md_sieve_RuleGradingCurve/Create
        public ActionResult Create()
        {
            ViewBag.SieveSetId = new SelectList(db.Md_sieve_SieveSet, "Id", "Description");
            ViewBag.GradingId = new SelectList(db.md_material_GradingGroup, "Id", "Description");
            return View();
        }

        // POST: Sieve/md_sieve_RuleGradingCurve/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Number,Description,SieveSetId,GradingId,A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,B13,B14,B15,C0,C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,C11,C12,C13,C14,C15,U0,U1,U2,U3,U4,U5,U6,U7,U8,U9,U10,U11,U12,U13,U14,U15")] md_sieve_RuleGradingCurve md_sieve_RuleGradingCurve)
        {
            if (ModelState.IsValid)
            {
                md_sieve_RuleGradingCurve.IsDeleted = false;
                md_sieve_RuleGradingCurve.IsActive = true;
                db.Md_sieve_RuleGradingCurve.Add(md_sieve_RuleGradingCurve);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.SieveSetId = new SelectList(db.Md_sieve_SieveSet, "Id", "Description", md_sieve_RuleGradingCurve.SieveSetId);
            ViewBag.GradingId = new SelectList(db.md_material_GradingGroup, "Id", "Description", md_sieve_RuleGradingCurve.GradingId);
            return View(md_sieve_RuleGradingCurve);
        }

        // GET: Sieve/md_sieve_RuleGradingCurve/Edit/5
        public ActionResult Edit(long? id)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_sieve_RuleGradingCurve md_sieve_RuleGradingCurve = db.Md_sieve_RuleGradingCurve.Find(id);
            if (md_sieve_RuleGradingCurve == null)
            {
                return HttpNotFound();
            }
            ViewBag.SieveSetId = new SelectList(db.Md_sieve_SieveSet, "Id", "Description", md_sieve_RuleGradingCurve.SieveSetId);            
            ViewBag.GradingId = new SelectList(db.md_material_GradingGroup, "Id", "Description", md_sieve_RuleGradingCurve.GradingId);
            md_sieve_SieveSet t = db.Md_sieve_SieveSet.Find(md_sieve_RuleGradingCurve.SieveSetId);
            ViewBag.Sieves = t.Sieves.OrderBy(a => a.Size).Select(a => a.Size).ToList();

            return View(md_sieve_RuleGradingCurve);
        }

        // POST: Sieve/md_sieve_RuleGradingCurve/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Number,Description,SieveSetId,GradingId,A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,A15,B0,B1,B2,B3,B4,B5,B6,B7,B8,B9,B10,B11,B12,B13,B14,B15,C0,C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,C11,C12,C13,C14,C15,U0,U1,U2,U3,U4,U5,U6,U7,U8,U9,U10,U11,U12,U13,U14,U15")] md_sieve_RuleGradingCurve md_sieve_RuleGradingCurve)
        {
            if (ModelState.IsValid)
            {
                var logger = new MyLogger();
                db.Database.Log = s => logger.Log("EFApp", s);
                db.Entry(md_sieve_RuleGradingCurve).State = EntityState.Modified;
                md_sieve_RuleGradingCurve.IsActive = true;
                md_sieve_RuleGradingCurve.IsDeleted = false;

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SieveSetId = new SelectList(db.Md_sieve_SieveSet, "Id", "Description", md_sieve_RuleGradingCurve.SieveSetId);
            ViewBag.GradingId = new SelectList(db.md_material_GradingGroup, "Id", "Description", md_sieve_RuleGradingCurve.GradingId);
            md_sieve_SieveSet t = db.Md_sieve_SieveSet.Find(md_sieve_RuleGradingCurve.SieveSetId);
            ViewBag.Sieves = t.Sieves.OrderBy(a => a.Size).Select(a => a.Size).ToList();
            return View(md_sieve_RuleGradingCurve);
        }

        // GET: Sieve/md_sieve_RuleGradingCurve/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_sieve_RuleGradingCurve md_sieve_RuleGradingCurve = db.Md_sieve_RuleGradingCurve.Find(id);
            if (md_sieve_RuleGradingCurve == null)
            {
                return HttpNotFound();
            }
            return View(md_sieve_RuleGradingCurve);
        }

        // POST: Sieve/md_sieve_RuleGradingCurve/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            md_sieve_RuleGradingCurve md_sieve_RuleGradingCurve = db.Md_sieve_RuleGradingCurve.Find(id);
            md_sieve_RuleGradingCurve.IsDeleted = true;
            db.Entry(md_sieve_RuleGradingCurve).State = EntityState.Modified;
            //db.Md_sieve_RuleGradingCurve.Remove(md_sieve_RuleGradingCurve);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult GetSivesSet(int id)
        {
            md_sieve_SieveSet t = db.Md_sieve_SieveSet.Find(id);
            
            return Json(t.Sieves.OrderBy(a => a.Size).Select(a => a.Size).ToList());
        }
        public class MyLogger
        {
            public void Log(string component, string message)
            {
                Console.WriteLine("Component: {0} Message: {1} ", component, message);
            //    using (System.IO.StreamWriter file =
            //new System.IO.StreamWriter(@"C:\Users\Manuel\Documents\log.txt", true))
            //    {
            //        file.WriteLine(message);
            //    }
            }
        }

        //***************************************************************************Reporting
        public ActionResult ReportDetail(long Id)
        {
            try {
                var model = db.Md_sieve_RuleGradingCurve.Where(a => a.Id == Id).First();
            List<string> Sieves = Definitions.allSieves.ToList();

            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();
            List<decimal> A = ReportHelper.eliminateNull(model.getA);
            List<decimal> B = ReportHelper.eliminateNull(model.getB);
            List<decimal> C = ReportHelper.eliminateNull(model.getC);
            List<decimal> U = ReportHelper.eliminateNull(model.getU);

            int counter = 0;
                foreach (string s in Sieves)
            {
                ds.RuleGradingCurve.AddRuleGradingCurveRow(s, A[counter], B[counter], C[counter], U[counter]);
                counter++;
            }

            ReportDataSource DSReport = new ReportDataSource("ds", ds.RuleGradingCurve.ToList());

            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Title", "Siebsatz"));
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToShortDateString()));
            repParams.Add(new ReportParameter("Description", "Details des Siebesatzes"));
            repParams.Add(new ReportParameter("Footer", "Anzahl Siebe: " + Sieves.Count.ToString()));
            repParams.Add(new ReportParameter("Id", model.Id.ToString()));
            repParams.Add(new ReportParameter("Name", model.Description.ToString()));
            repParams.Add(new ReportParameter("Number", model.Number.ToString()));
            repParams.Add(new ReportParameter("SieveSet", model.SieveSetId.ToString()));
            repParams.Add(new ReportParameter("GradingGroup", model.GradingId.ToString()));

            //string SievesList = "";
            //for (int i = 0; i < Sieves.Count; i++)
            //{
            //    SievesList += Sieves[i].ToString() + ";";
            //}
            //for (int i = 0; i < (17 - Sieves.Count); i++)
            //{
            //    SievesList += "-;";
            //}

            //repParams.Add(new ReportParameter("SievesList", SievesList));

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/Sieve/RuleGradingCurveDetail.rdlc";
            //reportViewer.ShowPrintButton = false;
            //reportViewer.LocalReport.DataSources.Add(DSReportSieves);
            //reportViewer.LocalReport.DataSources.Add(DSReportA);
            //reportViewer.LocalReport.DataSources.Add(DSReportB);
            //reportViewer.LocalReport.DataSources.Add(DSReportC);
            //reportViewer.LocalReport.DataSources.Add(DSReportU);
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 1200;
            reportViewer.Height = 700;

            ViewBag.ReportViewer = reportViewer;

                return View("Report");
        }
            catch(Exception e)
            {                
                PCMS.Helper.ExceptionHelper.LogException(e,User.Identity.Name);
                return View("~/Views/Shared/Exception",e);
            }
        }

        public ActionResult Report(PCMS.Models.PaginationModel pg)
        {
            try { 
            var model = pageModel(pg);
            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();
            foreach (var dataset in model)
            {
                ds.List.AddListRow(dataset.Id.ToString(), dataset.Number.ToString(), dataset.Description.ToString(), dataset.md_sieve_SieveSet.Description.ToString(), dataset.md_material_GradinGroup.Description.ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            }
            ReportDataSource DSReport = new ReportDataSource("dsList", ds.List.ToList());

            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Header_00", "ID"));
            repParams.Add(new ReportParameter("Hide_00", "false"));
            repParams.Add(new ReportParameter("Header_01", "Nummer"));
            repParams.Add(new ReportParameter("Hide_01", "false"));
            repParams.Add(new ReportParameter("Header_02", "Name"));
            repParams.Add(new ReportParameter("Hide_02", "false"));
            repParams.Add(new ReportParameter("Header_03", "Siebsatz"));
            repParams.Add(new ReportParameter("Hide_03", "false"));
            repParams.Add(new ReportParameter("Header_04", "Korngruppe"));
            repParams.Add(new ReportParameter("Hide_04", "false"));
            repParams.Add(new ReportParameter("Header_05", ""));
            repParams.Add(new ReportParameter("Hide_05", "true"));
            repParams.Add(new ReportParameter("Header_06", ""));
            repParams.Add(new ReportParameter("Hide_06", "true"));
            repParams.Add(new ReportParameter("Header_07", ""));
            repParams.Add(new ReportParameter("Hide_07", "true"));
            repParams.Add(new ReportParameter("Header_08", ""));
            repParams.Add(new ReportParameter("Hide_08", "true"));
            repParams.Add(new ReportParameter("Header_09", ""));
            repParams.Add(new ReportParameter("Hide_09", "true"));

            repParams.Add(new ReportParameter("Title", "Regelsieblinien Liste"));
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToShortDateString()));
            repParams.Add(new ReportParameter("Description", "Liste der Regelsieblinien"));
            repParams.Add(new ReportParameter("Footer", "Gesamtzahl Einträge: " + model.Count()));

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/ListReport.rdlc";
        
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 1200;
            reportViewer.Height = 700;
            ViewBag.ReportViewer = reportViewer;

            return View();
            }
            catch (Exception e)
            {
                PCMS.Helper.ExceptionHelper.LogException(e, User.Identity.Name);
                return View("~/Views/Shared/Exception.cshtml",e);
            }
        }

        public ActionResult Report2(PCMS.Models.PaginationModel pg)
        {
            
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/test.rdl";

            //reportViewer.ShowPrintButton = false;
            

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 1200;
            reportViewer.Height = 700;

            ViewBag.ReportViewer = reportViewer;

            return PartialView("Report");
        }

        

        private List<md_sieve_RuleGradingCurve> pageModel(PCMS.Models.PaginationModel pg)
        {

            var model = db.Md_sieve_RuleGradingCurve.Include(m => m.md_sieve_SieveSet).Include(m => m.md_material_GradinGroup).ToList();
            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Number":
                                model = model.Where(m => m.Number != null && m.Number.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Number = pgFF.colVal;
                                break;
                            case "Description":
                                model = model.Where(m => m.Description != null && m.Description.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Description = pgFF.colVal;
                                break;
                            case "SieveSetId":
                                model = model.Where(m => m.SieveSetId.Equals(Convert.ToInt64(pgFF.colVal))).ToList();
                                ViewBag.SieveSetId = Convert.ToInt64(pgFF.colVal);
                                break;
                            case "GradingId":
                                model = model.Where(m => m.GradingId.Equals(Convert.ToInt64(pgFF.colVal))).ToList();
                                ViewBag.GradingId = Convert.ToInt64(pgFF.colVal);
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Number":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Number).ToList();
                    else
                        model = model.OrderByDescending(m => m.Number).ToList();
                    break;
                case "Description":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Description).ToList();
                    else
                        model = model.OrderByDescending(m => m.Description).ToList();
                    break;
                case "SieveSetId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.SieveSetId).ToList();
                    else
                        model = model.OrderByDescending(m => m.SieveSetId).ToList();
                    break;
                case "GradingId":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.GradingId).ToList();
                    else
                        model = model.OrderByDescending(m => m.GradingId).ToList();
                    break;

            }
            ViewBag.SieveSetId = new SelectList(db.Md_sieve_SieveSet, "Id", "Description", (ViewBag.SieveSetId ?? null));
            ViewBag.GradingId = new SelectList(db.md_material_GradingGroup, "Id", "Description", (ViewBag.GradingId ?? null));
            return model.ToList();
        }

    }
}
