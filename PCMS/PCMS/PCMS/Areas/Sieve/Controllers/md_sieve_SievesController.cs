﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.Sieve.Models;
using PagedList;
using PCMS.Helper;
using Microsoft.Reporting.WebForms;

namespace PCMS.Areas.Sieve.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_sieve_SievesController : Controller
    {
        private SieveContext db = new SieveContext();

        // GET: Sieve/md_sieve_Sieves
        public ActionResult Index(PCMS.Models.PaginationModel pg)
        {
            var model = db.Md_sieve_Sieves.ToList();
            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Size":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Size).ToList();
                    else
                        model = model.OrderByDescending(m => m.Size).ToList();
                    break;
                case "PassLimit":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.PassLimit).ToList();
                    else
                        model = model.OrderByDescending(m => m.PassLimit).ToList();
                    break;
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }


        // GET: Sieve/md_sieve_Sieves/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_sieve_Sieves md_sieve_Sieves = db.Md_sieve_Sieves.Find(id);
            if (md_sieve_Sieves == null)
            {
                return HttpNotFound();
            }
            return View(md_sieve_Sieves);
        }

        // GET: Sieve/md_sieve_Sieves/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Sieve/md_sieve_Sieves/Create
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Size,PassLimit")] md_sieve_Sieves md_sieve_Sieves)
        {
            if (ModelState.IsValid)
            {
                db.Md_sieve_Sieves.Add(md_sieve_Sieves);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(md_sieve_Sieves);
        }

        // GET: Sieve/md_sieve_Sieves/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_sieve_Sieves md_sieve_Sieves = db.Md_sieve_Sieves.Find(id);
            if (md_sieve_Sieves == null)
            {
                return HttpNotFound();
            }
            return View(md_sieve_Sieves);
        }

        // POST: Sieve/md_sieve_Sieves/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Size,PassLimit")] md_sieve_Sieves md_sieve_Sieves)
        {
            if (ModelState.IsValid)
            {
                db.Entry(md_sieve_Sieves).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(md_sieve_Sieves);
        }

        // GET: Sieve/md_sieve_Sieves/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_sieve_Sieves md_sieve_Sieves = db.Md_sieve_Sieves.Find(id);
            if (md_sieve_Sieves == null)
            {
                return HttpNotFound();
            }
            return View(md_sieve_Sieves);
        }

        // POST: Sieve/md_sieve_Sieves/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            md_sieve_Sieves md_sieve_Sieves = db.Md_sieve_Sieves.Find(id);
            db.Md_sieve_Sieves.Remove(md_sieve_Sieves);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //*****************************************************************Reporting

        public ActionResult Report(PCMS.Models.PaginationModel pg)
        {
            var model = pageModel(pg);
            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();
            md_sieve_Sieves blubb = new md_sieve_Sieves();
            string displayName = PCMS.Helper.ReportHelper.GetDisplayName(blubb, m => m.Id);
            bool isReq1 = PCMS.Helper.ReportHelper.GetIsReport(blubb, m => m.SieveSet);
            bool isReq2 = PCMS.Helper.ReportHelper.GetIsReport(blubb, m => m.Id);

            foreach (var dataset in model)
            {
                ds.List.AddListRow(dataset.Id.ToString(), dataset.Size.ToString(), dataset.PassLimit.ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            }
            ReportDataSource DSReport = new ReportDataSource("dsList", ds.List.ToList());

            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Header_00", "ID"));
            repParams.Add(new ReportParameter("Hide_00", "false"));
            repParams.Add(new ReportParameter("Header_01", "Größe"));
            repParams.Add(new ReportParameter("Hide_01", "false"));
            repParams.Add(new ReportParameter("Header_02", "Grenzwert"));
            repParams.Add(new ReportParameter("Hide_02", "false"));
            repParams.Add(new ReportParameter("Header_03", ""));
            repParams.Add(new ReportParameter("Hide_03", "true"));
            repParams.Add(new ReportParameter("Header_04", ""));
            repParams.Add(new ReportParameter("Hide_04", "true"));
            repParams.Add(new ReportParameter("Header_05", ""));
            repParams.Add(new ReportParameter("Hide_05", "true"));
            repParams.Add(new ReportParameter("Header_06", ""));
            repParams.Add(new ReportParameter("Hide_06", "true"));
            repParams.Add(new ReportParameter("Header_07", ""));
            repParams.Add(new ReportParameter("Hide_07", "true"));
            repParams.Add(new ReportParameter("Header_08", ""));
            repParams.Add(new ReportParameter("Hide_08", "true"));
            repParams.Add(new ReportParameter("Header_09", ""));
            repParams.Add(new ReportParameter("Hide_09", "true"));

            repParams.Add(new ReportParameter("Title", "Siebe"));
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToShortDateString()));
            repParams.Add(new ReportParameter("Description", "Liste der Siebe"));
            repParams.Add(new ReportParameter("Footer", "Gesamtzahl Einträge: " + model.Count()));

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/ListReport.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 1200;
            reportViewer.Height = 700;

            ViewBag.ReportViewer = reportViewer;

            return PartialView();
        }
        
        public ActionResult ReportDetail(long Id)
        {
            var model = db.Md_sieve_Sieves.Where(a => a.Id == Id);

            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Title", "Siebe"));
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToShortDateString()));
            repParams.Add(new ReportParameter("Description", "Details des Siebes"));
            repParams.Add(new ReportParameter("Footer", "Gesamtzahl Einträge: "));
            repParams.Add(new ReportParameter("Id", model.First().Id.ToString()));
            repParams.Add(new ReportParameter("Size", model.First().Size.ToString()));
            repParams.Add(new ReportParameter("PassLimit", model.First().PassLimit.ToString()));

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/Sieve/SievesDetail.rdlc";
            //reportViewer.ShowPrintButton = false;
            //reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 1200;
            reportViewer.Height = 700;

            ViewBag.ReportViewer = reportViewer;

            return PartialView("Report");
        }

        private List<md_sieve_Sieves> pageModel(PCMS.Models.PaginationModel pg)
        {
            var model = db.Md_sieve_Sieves.ToList();
            if (pg != null)
            {
                foreach (PCMS.Models.PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Size":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Size).ToList();
                    else
                        model = model.OrderByDescending(m => m.Size).ToList();
                    break;
                case "PassLimit":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.PassLimit).ToList();
                    else
                        model = model.OrderByDescending(m => m.PassLimit).ToList();
                    break;
            }
            return model;
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }


}
