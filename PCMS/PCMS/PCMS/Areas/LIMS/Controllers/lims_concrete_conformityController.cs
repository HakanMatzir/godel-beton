﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PCMS.Areas.LIMS.Controllers
{

    public class lims_concrete_conformityController : Controller
    {
        private Areas.MasterData.Models.MasterDataContext db = new MasterData.Models.MasterDataContext();
        private Areas.Order.Models.OrderContext dbOrder = new Order.Models.OrderContext();
        private Areas.LIMS.Models.LimsContext dbLims = new LIMS.Models.LimsContext();
        private Areas.Recipe.Models.RecipeContext dbRecipe = new Recipe.Models.RecipeContext();
        // GET: LIMS/lims_concrete_conformity
        public ActionResult Index(DateTime? from, DateTime? to, bool? print, List<long> showSortDetailsL, List<long> showTestDetalisL, List<long> showKr1L, List<long> showKr2L,List<long> showKr3L)
        {
            PCMSDataSet ds = new PCMSDataSet();
            var model = db.Md_masterData_ConcreteFamily.Where(a => a.IsDeleted != true);
            var confList = new List<conformityModel>();
            foreach (var fam in model)
            {
                DateTime dateFrom = from != null ? from.Value.Date : dbOrder.Md_order_LoadingOrder.Select(a => a.RegistrationDate).Min() ?? DateTime.Now.Date;
                DateTime dateTo = to != null ? to.Value.Date : dbOrder.Md_order_LoadingOrder.Select(a => a.RegistrationDate).Max() ?? DateTime.Now.Date;
                conformityModel confTmp = new conformityModel();
                confTmp.FamId = fam.Id;
                confTmp.Description = fam.Description;
                confTmp.Static = true;
                confTmp.Conform = false;
                confTmp.RefSortDescription = db.Md_masterData_Material.Where(a => a.Id == fam.ReferenceSort).FirstOrDefault().MaterialNumber + ", " + db.Md_masterData_Material.Where(a => a.Id == fam.ReferenceSort).FirstOrDefault().Name;
                var sorts = db.Md_material_SortDetails.Where(a => a.ConcreteFamilyId == fam.Id).Select(a => a.Id).ToList();
                var artikel = db.Md_masterData_Material.Where(a => sorts.Contains(a.SortId)).Select(a => a.Id).ToList();
                var artikelNumb = db.Md_masterData_Material.Where(a => sorts.Contains(a.SortId)).Select(a => a.ArticleNumber).ToList();
                var loadingOrder = dbOrder.Md_order_LoadingOrder.Where(a => artikel.Contains(a.MaterialId) && a.State == 4 && a.RegistrationDate <= dateTo && a.RegistrationDate >= dateFrom).ToList();
                confTmp.Amount = loadingOrder.Where(a => artikel.Contains(a.MaterialId) && a.State == 4).Select(a => a.OrderedQuantity).Sum() ?? 0;
                confTmp.ProduktionDays = loadingOrder.GroupBy(a => a.RegistrationDate.Value.Date).Count();
                confTmp.ProductionWeek = confTmp.ProduktionDays / 6;
                //testDate??
                confTmp.ProbsDone = dbLims.Lims_concrete_Test.Where(a => (a.Stauts == 2 || a.Stauts == 3) && artikel.Contains(a.SortId) && a.IsDeleted == false && a.TestDate <= dateTo && a.TestDate >= dateFrom).OrderByDescending(a => a.TestDate).Select(a => a.TestDate).Count();


                confTmp.ProbsToDo = confTmp.ProductionWeek;
                if ((confTmp.Amount / 400) > confTmp.ProductionWeek)
                    confTmp.ProbsToDo = Convert.ToInt32(Math.Floor((Convert.ToDouble(confTmp.Amount) / 400)));
                confList.Add(confTmp);
                if (print == true)
                {
                    bool showSortDetails = showSortDetailsL != null ? showSortDetailsL.Contains(fam.Id) : false;
                    bool showTestDetalis = showTestDetalisL != null ? showTestDetalisL.Contains(fam.Id) : false; 
                    bool showKr1 = showKr1L != null ? showKr1L.Contains(fam.Id) : false;
                    bool showKr2 = showKr2L != null ? showKr2L.Contains(fam.Id) : false;
                    bool showKr3 = showKr3L != null ? showKr3L.Contains(fam.Id) : false;

                    ds.Conformity.AddConformityRow(confTmp.Description, confTmp.Static, confTmp.Conform, confTmp.RefSortDescription, confTmp.Amount, confTmp.ProductionWeek, confTmp.ProduktionDays, confTmp.ProbsToDo, confTmp.ProbsDone, showSortDetails, "", "", "", "", 0, 0, 0, showTestDetalis, "", DateTime.Now, false, "", 0, "", "", 0, 0, 0, 0, 0, 0, 0, 0, "", 0, 0, 0,  0, showKr2, 0, false, showKr3, "", 0, 0, false, showKr1, 0, 0, 0, 0, false, "", 0);
                    showSortDetails = true;
                    showTestDetalis = false;
                    foreach (var mat in DetailsFamilySortsList(from, to, fam.Id))
                    {
                        ds.Conformity.AddConformityRow(confTmp.Description, false, false, "", 0, 0, 0, 0, 0, showSortDetails, mat.Description, mat.ExpositionClass, mat.StrenghtClass, mat.KonsistenceClass, mat.MaxGraiCorn, mat.Amount, mat.ProbsDone, showTestDetalis, "", DateTime.Now, false, "", 0, "", "", 0, 0, 0, 0, 0, 0, 0, 0, "", 0, 0, 0, 0, showKr2, 0, false, showKr3, "", 0, 0, false, showKr1, 0, 0, 0, 0, false, "", 0);
                    }
                    showSortDetails = false;
                    showTestDetalis = true;
                    foreach (var mat in DetailsTestsList(from, to, fam.Id))
                    {
                        ds.Conformity.AddConformityRow(confTmp.Description, false, false, "", 0, 0, 0, 0, 0, showSortDetails, "", "", "", "", 0, 0, 0, showTestDetalis, mat.Description, mat.TestDate, mat.Complete, mat.ProbNumber, mat.ProbCount, mat.OrderNumber, mat.Place, mat.AirConten, mat.WZValue, mat.Konsistency, mat.RawDensity, mat.GrossDensity, mat.AirTemp, mat.ConcreteTemp, mat.TestAge, mat.TestAgeUnit, mat.Strength, mat.StrengthToReach, mat.StrengthMin, mat.StrengthMax, showKr2, mat.Kr2Value, mat.Kr2Test, showKr3, mat.Kr3Count != null ? mat.Kr3Count.ToString() : "", mat.Kr3Value1 ?? 0, mat.Kr3Value2 ?? 0, mat.Kr3Test, showKr1, mat.Kr1Value1 ?? 0, mat.Kr1Value2 ?? 0, mat.Kr1Value3 ?? 0, mat.Kr1Value4 ?? 0, mat.Kr1Test, mat.StandardDeviationText, mat.StandardDeviation ?? 0);
                    }
                }

            }
            if (print == true)
            {
                return printPrepare(ds);
            }
            ViewBag.from = from != null ? from.Value.ToString("yyyy-MM-dd") : "";
            ViewBag.to = to != null ? to.Value.ToString("yyyy-MM-dd") : "";
            return View(confList);
        }
        public ActionResult DetailsFamilySorts(DateTime? from, DateTime? to, long famId)
        {
            var model = db.Md_masterData_ConcreteFamily.Where(a => a.IsDeleted != true && a.Id == famId).FirstOrDefault();
            var sorts = db.Md_material_SortDetails.Where(a => a.ConcreteFamilyId == model.Id).ToList();
            var sortsIds = sorts.Select(a => a.Id);
            var artikel = db.Md_masterData_Material.Where(a => sortsIds.Contains(a.SortId)).OrderBy(a => a.MaterialNumber).ToList();
            var artikelIds = artikel.Select(a => a.Id);
            DateTime dateFrom = from != null ? from.Value.Date : dbOrder.Md_order_LoadingOrder.Select(a => a.RegistrationDate).Min() ?? DateTime.Now.Date;
            DateTime dateTo = to != null ? to.Value.Date : dbOrder.Md_order_LoadingOrder.Select(a => a.RegistrationDate).Max() ?? DateTime.Now.Date;
            var loadingOrder = dbOrder.Md_order_LoadingOrder.Where(a => artikelIds.Contains(a.MaterialId) && a.State == 4 && a.RegistrationDate <= dateTo && a.RegistrationDate >= dateFrom).ToList();

            var confFamDetailList = new List<conformityFamDetailModel>();
            foreach (var art in artikel)
            {

                conformityFamDetailModel conformityFamDetailModel = new conformityFamDetailModel();
                conformityFamDetailModel.Amount = loadingOrder.Where(a => a.MaterialId == art.Id).Select(a => a.OrderedQuantity).Sum() ?? 0;
                if (conformityFamDetailModel.Amount == 0)
                    continue;
                var sort = sorts.Where(u => u.Id == art.SortId).FirstOrDefault();
                conformityFamDetailModel.Description = art.ArticleNumber + ", " + art.Name;
                conformityFamDetailModel.SortId = art.Id ?? 0;
                conformityFamDetailModel.ExpositionClass = db.Md_material_ExposureKombi.Where(a => a.Id == sort.ExposureKombiId).Select(b => b.ShortText).FirstOrDefault();
                conformityFamDetailModel.StrenghtClass = db.Md_material_CompressionStrenghtGroup.Where(a => a.Id == sort.CompressionId).Select(b => b.Class).FirstOrDefault();
                conformityFamDetailModel.KonsistenceClass = db.Md_material_ConsistencyGroup.Where(a => a.Id == sort.ConsistencyId).Select(b => b.Class).FirstOrDefault();
                conformityFamDetailModel.MaxGraiCorn = 12;
                conformityFamDetailModel.Amount = loadingOrder.Where(a => a.MaterialId == art.Id).Select(a => a.OrderedQuantity).Sum() ?? 0;


                //testDate??
                conformityFamDetailModel.ProbsDone = dbLims.Lims_concrete_Test.Where(a => (a.Stauts == 2 || a.Stauts == 3) && a.SortId == art.Id && a.IsDeleted == false && a.TestDate <= dateTo && a.TestDate >= dateFrom).OrderByDescending(a => a.TestDate).Select(a => a.TestDate).Count();
                confFamDetailList.Add(conformityFamDetailModel);
            }


            return View(confFamDetailList);
        }
        public List<conformityFamDetailModel> DetailsFamilySortsList(DateTime? from, DateTime? to, long famId)
        {
            var model = db.Md_masterData_ConcreteFamily.Where(a => a.IsDeleted != true && a.Id == famId).FirstOrDefault();
            var sorts = db.Md_material_SortDetails.Where(a => a.ConcreteFamilyId == model.Id).ToList();
            var sortsIds = sorts.Select(a => a.Id);
            var artikel = db.Md_masterData_Material.Where(a => sortsIds.Contains(a.SortId)).OrderBy(a => a.MaterialNumber).ToList();
            var artikelIds = artikel.Select(a => a.Id);
            DateTime dateFrom = from != null ? from.Value.Date : dbOrder.Md_order_LoadingOrder.Select(a => a.RegistrationDate).Min() ?? DateTime.Now.Date;
            DateTime dateTo = to != null ? to.Value.Date : dbOrder.Md_order_LoadingOrder.Select(a => a.RegistrationDate).Max() ?? DateTime.Now.Date;
            var loadingOrder = dbOrder.Md_order_LoadingOrder.Where(a => artikelIds.Contains(a.MaterialId) && a.State == 4 && a.RegistrationDate <= dateTo && a.RegistrationDate >= dateFrom).ToList();

            var confFamDetailList = new List<conformityFamDetailModel>();
            foreach (var art in artikel)
            {

                conformityFamDetailModel conformityFamDetailModel = new conformityFamDetailModel();
                conformityFamDetailModel.Amount = loadingOrder.Where(a => a.MaterialId == art.Id).Select(a => a.OrderedQuantity).Sum() ?? 0;
                if (conformityFamDetailModel.Amount == 0)
                    continue;
                var sort = sorts.Where(u => u.Id == art.SortId).FirstOrDefault();
                conformityFamDetailModel.Description = art.ArticleNumber + ", " + art.Name;
                conformityFamDetailModel.SortId = art.Id ?? 0;
                conformityFamDetailModel.ExpositionClass = db.Md_material_ExposureKombi.Where(a => a.Id == sort.ExposureKombiId).Select(b => b.ShortText).FirstOrDefault();
                conformityFamDetailModel.StrenghtClass = db.Md_material_CompressionStrenghtGroup.Where(a => a.Id == sort.CompressionId).Select(b => b.Class).FirstOrDefault();
                conformityFamDetailModel.KonsistenceClass = db.Md_material_ConsistencyGroup.Where(a => a.Id == sort.ConsistencyId).Select(b => b.Class).FirstOrDefault();
                conformityFamDetailModel.MaxGraiCorn = 12;
                conformityFamDetailModel.Amount = loadingOrder.Where(a => a.MaterialId == art.Id).Select(a => a.OrderedQuantity).Sum() ?? 0;


                //testDate??
                conformityFamDetailModel.ProbsDone = dbLims.Lims_concrete_Test.Where(a => (a.Stauts == 2 || a.Stauts == 3) && a.SortId == art.Id && a.IsDeleted == false && a.TestDate <= dateTo && a.TestDate >= dateFrom).OrderByDescending(a => a.TestDate).Select(a => a.TestDate).Count();
                confFamDetailList.Add(conformityFamDetailModel);
            }
            return confFamDetailList;
        }
        public ActionResult DetailsTests(DateTime? from, DateTime? to, long famId)
        {
            var model = db.Md_masterData_ConcreteFamily.Where(a => a.IsDeleted != true && a.Id == famId).FirstOrDefault();
            var sorts = db.Md_material_SortDetails.Where(a => a.ConcreteFamilyId == model.Id).ToList();

            var sortsIds = sorts.Select(a => a.Id);
            var artikel = db.Md_masterData_Material.Where(a => sortsIds.Contains(a.SortId)).OrderBy(a => a.MaterialNumber).ToList();
            var artikelIds = artikel.Select(a => a.Id);

            var refDetail = artikel.Where(l => l.Id == model.ReferenceSort).FirstOrDefault().SortId ?? 0;
            var refStrength = db.Md_material_CompressionStrenghtGroup.Where(a => a.Id == db.Md_material_SortDetails.Where(u => u.Id == refDetail).FirstOrDefault().CompressionId).FirstOrDefault();
            var refStrengthToReach = dbRecipe.Md_recipe_Recipe.Where(a => a.RecipeConcrete.Any(u => u.MaterialId == model.ReferenceSort)).FirstOrDefault().StrengthAfter28 ?? 0;
            DateTime dateFrom = from != null ? from.Value.Date : dbLims.Lims_concrete_Test.Select(a => a.TestDate).Min();
            DateTime dateTo = to != null ? to.Value.Date : dbLims.Lims_concrete_Test.Select(a => a.TestDate).Max();
            var tests = dbLims.Lims_concrete_Test.Where(a => (a.Stauts == 2 || a.Stauts == 3) && artikelIds.Contains(a.SortId) && a.IsDeleted == false && a.TestDate <= dateTo && a.TestDate >= dateFrom).OrderBy(a => a.TestDate);
            //var loadingOrder = dbOrder.Md_order_LoadingOrder.Where(a => artikelIds.Contains(a.MaterialId) && a.State == 4 && a.RegistrationDate <= dateTo && a.RegistrationDate >= dateFrom).ToList();
            decimal kr1Sum = 0;
            decimal kr1SumTmp = 0;
            var confFamDetailList = new List<conformityTestDetailModel>();
            int count = 1;
            List<decimal> standardDeviationList = new List<decimal>();


            foreach (var test in tests)
            {

                conformityTestDetailModel conformityTestDetailModel = new conformityTestDetailModel();
                //var sort = sorts.Where(u => u.Id == art.SortId).FirstOrDefault();
                conformityTestDetailModel.Description = artikel.Where(a => a.Id == test.SortId).FirstOrDefault().ArticleNumber;
                conformityTestDetailModel.TestId = test.Id;
                conformityTestDetailModel.SortId = test.SortId;
                conformityTestDetailModel.isRef = test.SortId == model.ReferenceSort ? true : false;
                conformityTestDetailModel.TestDate = test.TestDate;
                conformityTestDetailModel.Complete = test.Stauts == 3 ? true : false;
                conformityTestDetailModel.ProbNumber = test.TestNumber;
                conformityTestDetailModel.OrderNumber = test.OrderNumber;
                var detail = artikel.Where(l => l.Id == test.SortId).FirstOrDefault().SortId ?? 0;

                var strength = db.Md_material_CompressionStrenghtGroup.Where(a => a.Id == db.Md_material_SortDetails.Where(u => u.Id == detail).FirstOrDefault().CompressionId).FirstOrDefault();
                conformityTestDetailModel.StrengthMin = strength.Min ?? 0;
                conformityTestDetailModel.StrengthMax = strength.Max ?? 0;
                conformityTestDetailModel.StrengthToReach = dbRecipe.Md_recipe_Recipe.Where(a => a.RecipeConcrete.Any(u => u.MaterialId == test.SortId)).FirstOrDefault().StrengthAfter28 ?? 0;
                //conformityTestDetailModel.Place = test.;
                conformityTestDetailModel.ProbCount = test.Lims_concrete_ProbDrys.Where(a => a.TestAgeValue == "28").Count();
                if (test.Lims_concrete_ProbWets.Count > 0)
                {
                    conformityTestDetailModel.WZValue = test.Lims_concrete_ProbWets.Select(a => a.WZEQValue).Average();
                    conformityTestDetailModel.Konsistency = Convert.ToInt32(test.Lims_concrete_ProbWets.Select(a => a.Lims_concrete_ProbWet_Consistency.FirstOrDefault().ConsistencyValue).Average() ?? 0);
                    conformityTestDetailModel.RawDensity = Convert.ToInt32(test.Lims_concrete_ProbWets.Select(a => a.IsRawDensity).Average());
                    conformityTestDetailModel.AirTemp = Convert.ToInt32(test.Lims_concrete_ProbWets.Select(a => a.AirTemp).Average());
                    conformityTestDetailModel.ConcreteTemp = Convert.ToInt32(test.Lims_concrete_ProbWets.Select(a => a.ConcreteTemp).Average());
                }
                if (test.Lims_concrete_ProbDrys.Where(a => a.TestAgeValue == "28").Count() > 0)
                {
                    conformityTestDetailModel.GrossDensity = Convert.ToInt32(test.Lims_concrete_ProbDrys.Where(a => a.TestAgeValue == "28").Select(a => a.GrossDensity).Average());
                    conformityTestDetailModel.TestAge = Convert.ToInt32(test.Lims_concrete_ProbDrys.Where(a => a.TestAgeValue == "28").Select(a => a.TestAgeValue).FirstOrDefault());
                    conformityTestDetailModel.TestAgeUnit = test.Lims_concrete_ProbDrys.Where(a => a.TestAgeValue == "28").Select(a => a.TestAgeType).FirstOrDefault();
                    conformityTestDetailModel.Strength = test.Lims_concrete_ProbDrys.Where(a => a.TestAgeValue == "28").Select(a => a.Strength).Average() ?? 0;

                }
                //Kriterium 2
                conformityTestDetailModel.Kr2Value = conformityTestDetailModel.StrengthMax - 4;
                conformityTestDetailModel.Kr2Test = conformityTestDetailModel.Strength >= conformityTestDetailModel.Kr2Value ? true : false;

                //Kriterium 3
                conformityTestDetailModel.Kr3Test = true;
                conformityTestDetailModel.Kr3Count = 1;
                if (tests.Where(a => a.TestDate <= test.TestDate && a.SortId == test.SortId && a.Lims_concrete_ProbDrys.Any(u => u.TestAgeValue == "28")).Count() > 1)
                {
                    var k3test = tests.Where(a => a.TestDate <= test.TestDate && a.SortId == test.SortId && a.Lims_concrete_ProbDrys.Any(u => u.TestAgeValue == "28"));
                    conformityTestDetailModel.Kr3Value1 = 0;
                    foreach (var avg in k3test)
                    {
                        conformityTestDetailModel.Kr3Value1 += avg.Lims_concrete_ProbDrys.Select(a => a.Strength).Average() ?? 0;
                    }
                    conformityTestDetailModel.Kr3Value1 /= k3test.Count();
                    conformityTestDetailModel.Kr3Count = k3test.Count();
                    if (conformityTestDetailModel.Kr3Count == 2)
                        conformityTestDetailModel.Kr3Value2 = conformityTestDetailModel.StrengthMax - 1;
                    else if (conformityTestDetailModel.Kr3Count == 3)
                        conformityTestDetailModel.Kr3Value2 = conformityTestDetailModel.StrengthMax + 1;
                    else if (conformityTestDetailModel.Kr3Count == 4)
                        conformityTestDetailModel.Kr3Value2 = conformityTestDetailModel.StrengthMax + 2;
                    else if (conformityTestDetailModel.Kr3Count == 5)
                        conformityTestDetailModel.Kr3Value2 = conformityTestDetailModel.StrengthMax + Convert.ToDecimal(2.5);
                    else if (conformityTestDetailModel.Kr3Count > 5)
                        conformityTestDetailModel.Kr3Value2 = conformityTestDetailModel.StrengthMax + 3;
                    if (conformityTestDetailModel.Kr3Count > 14)
                        conformityTestDetailModel.Kr3Value2 = conformityTestDetailModel.StrengthMax + 3;
                    conformityTestDetailModel.Kr3Test = conformityTestDetailModel.Kr3Value1 >= conformityTestDetailModel.Kr3Value2 ? true : false;
                }
                //Kriterium 1
                conformityTestDetailModel.Kr1Value1 = conformityTestDetailModel.Strength - conformityTestDetailModel.StrengthToReach;
                conformityTestDetailModel.Kr1Value2 = refStrengthToReach + conformityTestDetailModel.Kr1Value1;
                standardDeviationList.Add(conformityTestDetailModel.Kr1Value2 ?? 0);
                kr1Sum += conformityTestDetailModel.Kr1Value2 ?? 0;
                kr1SumTmp += conformityTestDetailModel.Kr1Value2 ?? 0;
                if (count % 3 == 0)
                {
                    conformityTestDetailModel.Kr1Value3 = kr1SumTmp / 3;
                    kr1SumTmp = 0;
                    conformityTestDetailModel.Kr1Value4 = refStrength.Max + 4;
                    conformityTestDetailModel.Kr1Test = conformityTestDetailModel.Kr1Value3 >= conformityTestDetailModel.Kr1Value4 ? true : false;
                }
                if (count >= 12 && count % 3 == 0)
                {
                    double ret = 0;
                    if (standardDeviationList.Count() > 0)
                    {
                        //Compute the Average      
                        decimal avg = standardDeviationList.Average();
                        //Perform the Sum of (value-avg)_2_2      
                        double sum = standardDeviationList.Sum(d => Math.Pow(Convert.ToDouble(d - avg), 2));
                        //Put it all together      
                        ret = Math.Sqrt((sum) / (standardDeviationList.Count() - 1));
                    }
                    conformityTestDetailModel.StandardDeviation = Math.Round(ret, 2);
                    conformityTestDetailModel.StandardDeviationText = standardDeviationList.Count().ToString();
                }

                //testDate??
                //conformityTestDetailModel.ProbsDone = dbLims.Lims_concrete_Test.Where(a => (a.Stauts == 2 || a.Stauts == 3) && a.SortId == art.Id && a.IsDeleted == false && a.TestDate <= dateTo && a.TestDate >= dateFrom).OrderByDescending(a => a.TestDate);
                confFamDetailList.Add(conformityTestDetailModel);
                count++;
            }


            return View(confFamDetailList);
        }
        public List<conformityTestDetailModel> DetailsTestsList(DateTime? from, DateTime? to, long famId)
        {
            var model = db.Md_masterData_ConcreteFamily.Where(a => a.IsDeleted != true && a.Id == famId).FirstOrDefault();
            var sorts = db.Md_material_SortDetails.Where(a => a.ConcreteFamilyId == model.Id).ToList();

            var sortsIds = sorts.Select(a => a.Id);
            var artikel = db.Md_masterData_Material.Where(a => sortsIds.Contains(a.SortId)).OrderBy(a => a.MaterialNumber).ToList();
            var artikelIds = artikel.Select(a => a.Id);

            var refDetail = artikel.Where(l => l.Id == model.ReferenceSort).FirstOrDefault().SortId ?? 0;
            var refStrength = db.Md_material_CompressionStrenghtGroup.Where(a => a.Id == db.Md_material_SortDetails.Where(u => u.Id == refDetail).FirstOrDefault().CompressionId).FirstOrDefault();
            var refStrengthToReach = dbRecipe.Md_recipe_Recipe.Where(a => a.RecipeConcrete.Any(u => u.MaterialId == model.ReferenceSort)).FirstOrDefault().StrengthAfter28 ?? 0;
            DateTime dateFrom = from != null ? from.Value.Date : dbLims.Lims_concrete_Test.Select(a => a.TestDate).Min();
            DateTime dateTo = to != null ? to.Value.Date : dbLims.Lims_concrete_Test.Select(a => a.TestDate).Max();
            var tests = dbLims.Lims_concrete_Test.Where(a => (a.Stauts == 2 || a.Stauts == 3) && artikelIds.Contains(a.SortId) && a.IsDeleted == false && a.TestDate <= dateTo && a.TestDate >= dateFrom).OrderBy(a => a.TestDate);
            //var loadingOrder = dbOrder.Md_order_LoadingOrder.Where(a => artikelIds.Contains(a.MaterialId) && a.State == 4 && a.RegistrationDate <= dateTo && a.RegistrationDate >= dateFrom).ToList();
            decimal kr1Sum = 0;
            decimal kr1SumTmp = 0;
            var confFamDetailList = new List<conformityTestDetailModel>();
            int count = 1;
            List<decimal> standardDeviationList = new List<decimal>();


            foreach (var test in tests)
            {

                conformityTestDetailModel conformityTestDetailModel = new conformityTestDetailModel();
                //var sort = sorts.Where(u => u.Id == art.SortId).FirstOrDefault();
                conformityTestDetailModel.Description = artikel.Where(a => a.Id == test.SortId).FirstOrDefault().ArticleNumber;
                conformityTestDetailModel.TestId = test.Id;
                conformityTestDetailModel.SortId = test.SortId;
                conformityTestDetailModel.isRef = test.SortId == model.ReferenceSort ? true : false;
                conformityTestDetailModel.TestDate = test.TestDate;
                conformityTestDetailModel.Complete = test.Stauts == 3 ? true : false;
                conformityTestDetailModel.ProbNumber = test.TestNumber;
                conformityTestDetailModel.OrderNumber = test.OrderNumber;
                var detail = artikel.Where(l => l.Id == test.SortId).FirstOrDefault().SortId ?? 0;

                var strength = db.Md_material_CompressionStrenghtGroup.Where(a => a.Id == db.Md_material_SortDetails.Where(u => u.Id == detail).FirstOrDefault().CompressionId).FirstOrDefault();
                conformityTestDetailModel.StrengthMin = strength.Min ?? 0;
                conformityTestDetailModel.StrengthMax = strength.Max ?? 0;
                conformityTestDetailModel.StrengthToReach = dbRecipe.Md_recipe_Recipe.Where(a => a.RecipeConcrete.Any(u => u.MaterialId == test.SortId)).FirstOrDefault().StrengthAfter28 ?? 0;
                //conformityTestDetailModel.Place = test.;
                conformityTestDetailModel.ProbCount = test.Lims_concrete_ProbDrys.Where(a => a.TestAgeValue == "28").Count();
                if (test.Lims_concrete_ProbWets.Count > 0)
                {
                    conformityTestDetailModel.WZValue = test.Lims_concrete_ProbWets.Select(a => a.WZEQValue).Average();
                    conformityTestDetailModel.Konsistency = Convert.ToInt32(test.Lims_concrete_ProbWets.Select(a => a.Lims_concrete_ProbWet_Consistency.FirstOrDefault().ConsistencyValue).Average() ?? 0);
                    conformityTestDetailModel.RawDensity = Convert.ToInt32(test.Lims_concrete_ProbWets.Select(a => a.IsRawDensity).Average());
                    conformityTestDetailModel.AirTemp = Convert.ToInt32(test.Lims_concrete_ProbWets.Select(a => a.AirTemp).Average());
                    conformityTestDetailModel.ConcreteTemp = Convert.ToInt32(test.Lims_concrete_ProbWets.Select(a => a.ConcreteTemp).Average());
                }
                if (test.Lims_concrete_ProbDrys.Where(a => a.TestAgeValue == "28").Count() > 0)
                {
                    conformityTestDetailModel.GrossDensity = Convert.ToInt32(test.Lims_concrete_ProbDrys.Where(a => a.TestAgeValue == "28").Select(a => a.GrossDensity).Average());
                    conformityTestDetailModel.TestAge = Convert.ToInt32(test.Lims_concrete_ProbDrys.Where(a => a.TestAgeValue == "28").Select(a => a.TestAgeValue).FirstOrDefault());
                    conformityTestDetailModel.TestAgeUnit = test.Lims_concrete_ProbDrys.Where(a => a.TestAgeValue == "28").Select(a => a.TestAgeType).FirstOrDefault();
                    conformityTestDetailModel.Strength = test.Lims_concrete_ProbDrys.Where(a => a.TestAgeValue == "28").Select(a => a.Strength).Average() ?? 0;

                }
                //Kriterium 2
                conformityTestDetailModel.Kr2Value = conformityTestDetailModel.StrengthMax - 4;
                conformityTestDetailModel.Kr2Test = conformityTestDetailModel.Strength >= conformityTestDetailModel.Kr2Value ? true : false;

                //Kriterium 3
                conformityTestDetailModel.Kr3Test = true;
                conformityTestDetailModel.Kr3Count = 1;
                if (tests.Where(a => a.TestDate <= test.TestDate && a.SortId == test.SortId && a.Lims_concrete_ProbDrys.Any(u => u.TestAgeValue == "28")).Count() > 1)
                {
                    var k3test = tests.Where(a => a.TestDate <= test.TestDate && a.SortId == test.SortId && a.Lims_concrete_ProbDrys.Any(u => u.TestAgeValue == "28"));
                    conformityTestDetailModel.Kr3Value1 = 0;
                    foreach (var avg in k3test)
                    {
                        conformityTestDetailModel.Kr3Value1 += avg.Lims_concrete_ProbDrys.Select(a => a.Strength).Average() ?? 0;
                    }
                    conformityTestDetailModel.Kr3Value1 /= k3test.Count();
                    conformityTestDetailModel.Kr3Count = k3test.Count();
                    if (conformityTestDetailModel.Kr3Count == 2)
                        conformityTestDetailModel.Kr3Value2 = conformityTestDetailModel.StrengthMax - 1;
                    else if (conformityTestDetailModel.Kr3Count == 3)
                        conformityTestDetailModel.Kr3Value2 = conformityTestDetailModel.StrengthMax + 1;
                    else if (conformityTestDetailModel.Kr3Count == 4)
                        conformityTestDetailModel.Kr3Value2 = conformityTestDetailModel.StrengthMax + 2;
                    else if (conformityTestDetailModel.Kr3Count == 5)
                        conformityTestDetailModel.Kr3Value2 = conformityTestDetailModel.StrengthMax + Convert.ToDecimal(2.5);
                    else if (conformityTestDetailModel.Kr3Count > 5)
                        conformityTestDetailModel.Kr3Value2 = conformityTestDetailModel.StrengthMax + 3;
                    if (conformityTestDetailModel.Kr3Count > 14)
                        conformityTestDetailModel.Kr3Value2 = conformityTestDetailModel.StrengthMax + 3;
                    conformityTestDetailModel.Kr3Test = conformityTestDetailModel.Kr3Value1 >= conformityTestDetailModel.Kr3Value2 ? true : false;
                }
                //Kriterium 1
                conformityTestDetailModel.Kr1Value1 = conformityTestDetailModel.Strength - conformityTestDetailModel.StrengthToReach;
                conformityTestDetailModel.Kr1Value2 = refStrengthToReach + conformityTestDetailModel.Kr1Value1;
                standardDeviationList.Add(conformityTestDetailModel.Kr1Value2 ?? 0);
                kr1Sum += conformityTestDetailModel.Kr1Value2 ?? 0;
                kr1SumTmp += conformityTestDetailModel.Kr1Value2 ?? 0;
                if (count % 3 == 0)
                {
                    conformityTestDetailModel.Kr1Value3 = kr1SumTmp / 3;
                    kr1SumTmp = 0;
                    conformityTestDetailModel.Kr1Value4 = refStrength.Max + 4;
                    conformityTestDetailModel.Kr1Test = conformityTestDetailModel.Kr1Value3 >= conformityTestDetailModel.Kr1Value4 ? true : false;
                }
                if (count >= 12 && count % 3 == 0)
                {
                    double ret = 0;
                    if (standardDeviationList.Count() > 0)
                    {
                        //Compute the Average      
                        decimal avg = standardDeviationList.Average();
                        //Perform the Sum of (value-avg)_2_2      
                        double sum = standardDeviationList.Sum(d => Math.Pow(Convert.ToDouble(d - avg), 2));
                        //Put it all together      
                        ret = Math.Sqrt((sum) / (standardDeviationList.Count() - 1));
                    }
                    conformityTestDetailModel.StandardDeviation = Math.Round(ret, 2);
                    conformityTestDetailModel.StandardDeviationText = standardDeviationList.Count().ToString();
                }

                //testDate??
                //conformityTestDetailModel.ProbsDone = dbLims.Lims_concrete_Test.Where(a => (a.Stauts == 2 || a.Stauts == 3) && a.SortId == art.Id && a.IsDeleted == false && a.TestDate <= dateTo && a.TestDate >= dateFrom).OrderByDescending(a => a.TestDate);
                confFamDetailList.Add(conformityTestDetailModel);
                count++;
            }


            return confFamDetailList;
        }
        public ActionResult printPrepare(PCMSDataSet ds)
        {





            List<ReportParameter> repParams = new List<ReportParameter>();
            //  repParams.Add(new ReportParameter("Date", DateTime.Now.ToString("dd.MM.yyyy")));
            //  repParams.Add(new ReportParameter("MaterialNumber", con.ArticleNumber));
            //  repParams.Add(new ReportParameter("MaterialName", con.Name));
            //  repParams.Add(new ReportParameter("Exposition", exposure));
            //  repParams.Add(new ReportParameter("StrengthClass", compression));
            //  repParams.Add(new ReportParameter("Consistence", consistency));
            //  repParams.Add(new ReportParameter("process", progress));
            //  repParams.Add(new ReportParameter("ProbNorm", testStandard + "\n" + sorage));
            //  repParams.Add(new ReportParameter("ProbNr", model.TestNumber));
            //  repParams.Add(new ReportParameter("TesterName", testerName));
            //


            ReportDataSource DSReport = new ReportDataSource("DataSet1", ds.Conformity.ToList());

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/LIMS/ConcreteConformityDetail.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);

            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 800;
            reportViewer.Height = 700;

            //------------------
            string mimeType = "";
            string encoding = "";
            string filenameExtension = "";
            string[] streamids = null;
            Warning[] warnings = null;

            byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
            return File(file, mimeType);
        }
        // GET: LIMS/lims_concrete_conformity/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: LIMS/lims_concrete_conformity/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LIMS/lims_concrete_conformity/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LIMS/lims_concrete_conformity/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: LIMS/lims_concrete_conformity/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LIMS/lims_concrete_conformity/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: LIMS/lims_concrete_conformity/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
    public class conformityModel
    {
        public long FamId { get; set; }
        [DisplayName("Betonfamilie")]
        public string Description { get; set; }
        [DisplayName("Stetig")]
        public bool Static { get; set; }
        [DisplayName("Konform")]
        public bool Conform { get; set; }
        [DisplayName("Referenzsorte")]
        public string RefSortDescription { get; set; }
        [DisplayName("Prod. Menge")]
        public decimal Amount { get; set; }
        [DisplayName("Prod. Wochen")]
        public int ProductionWeek { get; set; }
        [DisplayName("Prod. Tage")]
        public int ProduktionDays { get; set; }
        [DisplayName("Proben-Soll")]
        public int ProbsToDo { get; set; }
        [DisplayName("Proben-Ist")]
        public int ProbsDone { get; set; }

    }
    public class conformityFamDetailModel
    {
        public long SortId { get; set; }
        [DisplayName("Sorte")]
        public string Description { get; set; }
        [DisplayName("Expositions-Klassen")]
        public string ExpositionClass { get; set; }
        [DisplayName("Festigkeits-Klasse")]
        public string StrenghtClass { get; set; }
        [DisplayName("Kons-Klasse")]
        public string KonsistenceClass { get; set; }
        [DisplayName("Gr-Korn")]
        public int MaxGraiCorn { get; set; }
        [DisplayName("Menge")]
        public decimal Amount { get; set; }
        [DisplayName("Proben-Ist")]
        public int ProbsDone { get; set; }
    }
    public class conformityTestDetailModel
    {
        public long SortId { get; set; }
        public long TestId { get; set; }
        [DisplayName("Sortennr.")]
        public string Description { get; set; }
        [DisplayName("Datum")]
        public DateTime TestDate { get; set; }
        [DisplayName("Vollst")]
        public bool Complete { get; set; }
        [DisplayName("Nr.")]
        public string ProbNumber { get; set; }
        [DisplayName("Anz.")]
        public int ProbCount { get; set; }
        [DisplayName("LsNr")]
        public string OrderNumber { get; set; }
        [DisplayName("Ort")]
        public string Place { get; set; }
        [DisplayName("LP%")]
        public decimal AirConten { get; set; }
        [DisplayName("W/Zeq")]
        public decimal WZValue { get; set; }
        [DisplayName("Kons")]
        public int Konsistency { get; set; }
        [DisplayName("FR-Rohd\n[kg/m³]")]
        public int RawDensity { get; set; }
        [DisplayName("FE-Rohd\n[kg/m³]")]
        public int GrossDensity { get; set; }
        [DisplayName("Bet-Temp\n[°C]")]
        public int AirTemp { get; set; }
        [DisplayName("Luft-Temp\n\n[°C]")]
        public int ConcreteTemp { get; set; }
        [DisplayName("Alter")]
        public int TestAge { get; set; }
        [DisplayName("Dim")]
        public string TestAgeUnit { get; set; }
        [DisplayName("Druck Festigkeit")]
        public decimal Strength { get; set; }
        [DisplayName("Ziel Festigkeit")]
        public decimal StrengthToReach { get; set; }
        public decimal StrengthMin { get; set; }
        [DisplayName("Charakter Festigkeit")]
        public decimal StrengthMax { get; set; }
        //Kriterium 2 (Einzelwertung)
        [DisplayName("f\x2092-4")]
        public decimal Kr2Value { get; set; }
        [DisplayName(">=f(ck)-4")]
        public bool Kr2Test { get; set; }
        //Kriterium 3 (Bestätigungskriterium)
        public int? Kr3Count { get; set; }
        public decimal? Kr3Value1 { get; set; }
        public decimal? Kr3Value2 { get; set; }
        public bool Kr3Test { get; set; }
        //Kriterium 1 (Mittelwert)
        public decimal? Kr1Value1 { get; set; }
        public decimal? Kr1Value2 { get; set; }
        public decimal? Kr1Value3 { get; set; }
        public decimal? Kr1Value4 { get; set; }
        public bool Kr1Test { get; set; }
        //Standartabweichung
        public string StandardDeviationText { get; set; }
        public double? StandardDeviation { get; set; }

        public bool isRef { get; set; }



    }
}
