﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.MasterData.Models;
using PCMS.Models;
using PagedList;
using PCMS.Helper;

namespace PCMS.Areas.MasterData.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_masterData_ContactController : Controller
    {
        private MasterDataContext db = new MasterDataContext();

        // GET: Order/md_order_Contact
        public ActionResult Index(PaginationModel pg)
        {
            var model = db.Md_masterData_Contact.OrderBy(a => a.Id).ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!String.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Id":
                                model = model.Where(m => m.Id.ToString().Contains(pgFF.colVal)).ToList();
                                ViewBag.Id = pgFF.colVal;
                                break;
                            case "FirstName":
                                model = model.Where(m => m.FirstName != null && m.FirstName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FirstName = pgFF.colVal;
                                break;
                            case "LastName":
                                model = model.Where(m => m.LastName != null && m.LastName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.LastName = pgFF.colVal;
                                break;
                            case "Mobile":
                                model = model.Where(m => m.Mobile != null && m.Mobile.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Mobile = pgFF.colVal;
                                break;
                            case "Tel":
                                model = model.Where(m => m.Tel != null && m.Tel.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Tel = pgFF.colVal;
                                break;
                            case "Fax":
                                model = model.Where(m => m.Fax != null && m.Fax.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Fax = pgFF.colVal;
                                break;
                            case "Email":
                                model = model.Where(m => m.Email != null && m.Email.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.Email = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "FirstName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.FirstName).ToList();
                    else
                        model = model.OrderByDescending(m => m.FirstName).ToList();
                    break;
                case "LastName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.LastName).ToList();
                    else
                        model = model.OrderByDescending(m => m.LastName).ToList();
                    break;
                case "Mobile":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Mobile).ToList();
                    else
                        model = model.OrderByDescending(m => m.Mobile).ToList();
                    break;
                case "Tel":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Tel).ToList();
                    else
                        model = model.OrderByDescending(m => m.Tel).ToList();
                    break;
                case "Fax":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Fax).ToList();
                    else
                        model = model.OrderByDescending(m => m.Fax).ToList();
                    break;
                case "Email":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Email).ToList();
                    else
                        model = model.OrderByDescending(m => m.Email).ToList();
                    break;
            }
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }


        // GET: Order/md_order_Contact/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_masterData_Contact md_masterData_Contact = db.Md_masterData_Contact.Find(id);
            if (md_masterData_Contact == null)
            {
                return HttpNotFound();
            }
            return View(md_masterData_Contact);
        }


        public ActionResult Create()
        {
            return View(new MasterDataContactView());
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                //db.Md_masterData_Contact.Add(md_masterData_Contact);
                db.Md_masterData_Contact.Add(model.contact);
                db.Md_masterData_Address.Add(model.address);
                db.Md_masterData_Company.Add(model.company);
                //db.md_masterData_Vehicle.Add(model.vehicle);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Order/md_order_Contact/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_masterData_Contact md_masterData_Contact = db.Md_masterData_Contact.Find(id);
            if (md_masterData_Contact == null)
            {
                return HttpNotFound();
            }
            return View(md_masterData_Contact);
        }

        // POST: Order/md_order_Contact/Edit/5
        // Aktivieren Sie zum Schutz vor übermäßigem Senden von Angriffen die spezifischen Eigenschaften, mit denen eine Bindung erfolgen soll. Weitere Informationen 
        // finden Sie unter http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,Mobile,Tel,Fax,Email")] md_masterData_Contact md_masterData_Contact)
        {
            if (ModelState.IsValid)
            {
                db.Entry(md_masterData_Contact).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(md_masterData_Contact);
        }

        // GET: MasterData/md_order_Contact/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            md_masterData_Contact md_masterData_Contact = db.Md_masterData_Contact.Find(id);
            if (md_masterData_Contact == null)
            {
                return HttpNotFound();
            }
            return View(md_masterData_Contact);
        }

        // POST: MasterData/md_order_Contact/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            md_masterData_Contact md_masterData_Contact = db.Md_masterData_Contact.Find(id);
            db.Md_masterData_Contact.Remove(md_masterData_Contact);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
