﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.MasterData.Models;
using PagedList;
using PCMS.Models;
using PCMS.Helper;

namespace PCMS.Areas.MasterData.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_masterData_AddressController : Controller
    {
        private MasterDataContext db = new MasterDataContext();

        //
        // GET: /masterData/AddressId/

        //public ActionResult Index(int? page, int? pagesize, List<PaginationModels> pagination,string masterDataDir, string masterDataCol)
        public ActionResult Index(PaginationModel pg)
        {
            
            var model = db.Md_masterData_Address.ToList();

            model = model.OrderBy(a => a.City).ToList();
            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "City":
                                model = model.Where(m => m.City.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ZipCode":
                                model = model.Where(m => m.ZipCode.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterZipCode = pgFF.colVal;
                                break;
                        }
                    }
                }
                
            }
            switch (pg.masterDataCol)
            {
                case "Id":
                    if (pg.masterDataDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "City":
                    if (pg.masterDataDir.Equals("desc"))
                        model = model.OrderBy(m => m.City).ToList();
                    else
                        model = model.OrderByDescending(m => m.City).ToList();
                    break;
                case "ZipCode":
                    if (pg.masterDataDir.Equals("desc"))
                        model = model.OrderBy(m => m.ZipCode).ToList();
                    else
                        model = model.OrderByDescending(m => m.ZipCode).ToList();
                    break;
            }

            

            
            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        //
        // GET: /masterData/AddressId/Details/5

        public ActionResult Details(long id = 0)
        {
            md_masterData_Address md_masterData_address = db.Md_masterData_Address.Find(id);
            if (md_masterData_address == null)
            {
                return HttpNotFound();
            }
            return View(md_masterData_address);
        }

        //
        // GET: /masterData/AddressId/Create

        public ActionResult Create()
        {
            return View(new MasterDataContactView());
        }

        //
        // POST: /masterData/AddressId/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                //db.Md_masterData_Contact.Add(md_masterData_Contact);
                db.Md_masterData_Contact.Add(model.contact);
                db.Md_masterData_Address.Add(model.address);
                db.Md_masterData_Company.Add(model.company);
                //db.md_masterData_Vehicle.Add(model.vehicle);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        //
        // GET: /masterData/AddressId/Edit/5

        public ActionResult Edit(long id = 0)
        {
            md_masterData_Address md_masterData_address = db.Md_masterData_Address.Find(id);
            if (md_masterData_address == null)
            {
                return HttpNotFound();
            }
            return View(md_masterData_address);
        }

        //
        // POST: /masterData/AddressId/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(md_masterData_Address md_masterData_address)
        {
            if (ModelState.IsValid)
            {
                db.Entry(md_masterData_address).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(md_masterData_address);
        }

        //
        // GET: /masterData/AddressId/Delete/5

        public ActionResult Delete(long id = 0)
        {
            md_masterData_Address md_masterData_address = db.Md_masterData_Address.Find(id);
            if (md_masterData_address == null)
            {
                return HttpNotFound();
            }
            return View(md_masterData_address);
        }

        //
        // POST: /masterData/AddressId/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            md_masterData_Address md_masterData_address = db.Md_masterData_Address.Find(id);
            db.Md_masterData_Address.Remove(md_masterData_address);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
        [HttpPost]
        public ActionResult addressTemplate(string filter)
        {
            return Json(db.Md_masterData_AddressTamplate.Where(m => m.City.Contains(filter)).ToList());

        }

    }
}
