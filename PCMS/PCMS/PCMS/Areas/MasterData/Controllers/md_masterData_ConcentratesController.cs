﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.MasterData.Models;
using PagedList;
using PCMS.Models;
using PCMS.Helper;
using Microsoft.Reporting.WebForms;
using Newtonsoft.Json;

namespace PCMS.Areas.MasterData.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_masterData_ConcentratesController : Controller
    {
        MasterDataContext db = new MasterDataContext();
         
        // GET:
        public ActionResult Index(PaginationModel pg)
        {
            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 7).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => m.ShortName != null && m.ShortName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "MaterialNumber":
                                model = model.Where(m => m.MaterialNumber != null && m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber != null && m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "MaterialNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }

            ViewBag.Index = "all";
            return View("Index", model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        // Get
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.MaterialGroupId = 7;
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Md_material_Concentrates.Add(model.concentrates);
                db.SaveChanges();
                model.material.ConcentratesId = model.concentrates.Id;
                db.Md_masterData_Material.Add(model.material);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View("Create", model);
        }

        // GET
        public ActionResult Edit(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_masterData_Material.Find(id) == null)
            {
                return HttpNotFound();
            }
            else
            {
                model.material = db.Md_masterData_Material.Find(id);
                model.deliverer = db.Md_masterData_Deliverer.Find(model.material.DelivererId);
                model.facili = db.Facilities.Find(model.material.FacilitiesId);
                model.materialType = db.Md_material_MaterialType.Find(model.material.MaterialTypeId);

                model.concentrates = db.Md_material_Concentrates.Find(model.material.ConcentratesId);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MasterDataContactView model)
        {
            if (ModelState.IsValid)
            {
                model.material.IsActive = true;
                model.material.IsDeleted = false;
                db.Entry(model.material).State = EntityState.Modified;
                db.Entry(model.concentrates).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET
        public ActionResult Delete(MasterDataContactView model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            model.material = db.Md_masterData_Material.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            ViewBag.RecipeCheck = db.Md_recipeMaterialM.Where(a => a.MaterialId == id && a.Md_recipe_Recipe.IsActive == true && a.Md_recipe_Recipe.IsDeleted == false).Select(a => a.Md_recipe_Recipe.Number).ToList().Count();
            ViewBag.RecipeNumber = db.Md_recipeMaterialM.Where(a => a.MaterialId == id && a.Md_recipe_Recipe.IsActive == true && a.Md_recipe_Recipe.IsDeleted == false).Select(a => a.Md_recipe_Recipe.Number).ToList();
            return View(model);
        }

        // POST
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(MasterDataContactView model, long id)
        {
            model.material.IsActive = true;
            model.material.IsDeleted = true;
            db.Entry(model.material).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index");
            
        }
        public ActionResult Report(string pgS)
        {
            PCMS.Models.PaginationModel pg = JsonConvert.DeserializeObject<PCMS.Models.PaginationModel>(pgS);
            var model = db.Md_masterData_Material.Where(m => m.IsActive == true && m.IsDeleted == false && m.MaterialGroupId == 7).OrderBy(a => a.Id).ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Name":
                                model = model.Where(m => m.Name != null && m.Name.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ShortName":
                                model = model.Where(m => m.ShortName != null && m.ShortName.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "MaterialNumber":
                                model = model.Where(m => m.MaterialNumber != null && m.MaterialNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ArticleNumber":
                                model = model.Where(m => m.ArticleNumber != null && m.ArticleNumber.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }
            }
            switch (pg.orderCol)
            {
                case "MaterialNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.MaterialNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.MaterialNumber).ToList();
                    break;
                case "Name":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Name).ToList();
                    else
                        model = model.OrderByDescending(m => m.Name).ToList();
                    break;
                case "ShortName":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortName).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortName).ToList();
                    break;
                case "ArticleNumber":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ArticleNumber).ToList();
                    else
                        model = model.OrderByDescending(m => m.ArticleNumber).ToList();
                    break;
            }

            PCMSDataSet ds = new PCMSDataSet();
            ds.List.Clear();
            foreach (var dataset in model)
            {
                ds.List.AddListRow(dataset.Id.ToString(), dataset.MaterialNumber, dataset.ArticleNumber.ToString(), dataset.Name.ToString(), dataset.ShortName.ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            }
            ReportDataSource DSReport = new ReportDataSource("dsList", ds.List.ToList());

            List<ReportParameter> repParams = new List<ReportParameter>();
            repParams.Add(new ReportParameter("Header_00", "ID"));
            repParams.Add(new ReportParameter("Hide_00", "true"));
            repParams.Add(new ReportParameter("Header_01", "Materialnummer"));
            repParams.Add(new ReportParameter("Hide_01", "false"));
            repParams.Add(new ReportParameter("Header_02", "Artikelnummer"));
            repParams.Add(new ReportParameter("Hide_02", "false"));
            repParams.Add(new ReportParameter("Header_03", "Bezeichnung"));
            repParams.Add(new ReportParameter("Hide_03", "false"));
            repParams.Add(new ReportParameter("Header_04", "Kurz-Bezeichnung"));
            repParams.Add(new ReportParameter("Hide_04", "false"));
            repParams.Add(new ReportParameter("Header_05", "Kurz-Bezeichnung"));
            repParams.Add(new ReportParameter("Hide_05", "true"));
            repParams.Add(new ReportParameter("Header_06", "Aktiv"));
            repParams.Add(new ReportParameter("Hide_06", "true"));
            repParams.Add(new ReportParameter("Header_07", ""));
            repParams.Add(new ReportParameter("Hide_07", "true"));
            repParams.Add(new ReportParameter("Header_08", ""));
            repParams.Add(new ReportParameter("Hide_08", "true"));
            repParams.Add(new ReportParameter("Header_09", ""));
            repParams.Add(new ReportParameter("Hide_09", "true"));

            repParams.Add(new ReportParameter("Title", "Material"));
            repParams.Add(new ReportParameter("Date", DateTime.Now.ToShortDateString()));
            repParams.Add(new ReportParameter("Description", "Konzentrate Übersicht"));
            repParams.Add(new ReportParameter("Footer", "Gesamtzahl Einträge: " + model.Count()));

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.LocalReport.ReportPath = "Reports/MasterData/ListReport.rdlc";
            //reportViewer.ShowPrintButton = false;
            reportViewer.LocalReport.DataSources.Add(DSReport);
            reportViewer.LocalReport.SetParameters(repParams);

            reportViewer.ShowPrintButton = true;
            reportViewer.ShowParameterPrompts = true;
            reportViewer.ShowBackButton = true;
            //reportViewer.DocumentMapWidth = 1000;
            reportViewer.Width = 800;
            reportViewer.Height = 700;

            //------------------
            string mimeType = "";
            string encoding = "";
            string filenameExtension = "";
            string[] streamids = null;
            Warning[] warnings = null;

            byte[] file = reportViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
            return File(file, mimeType);
            //ViewBag.ReportViewer = reportViewer;
            //return View();


        }
    }
}