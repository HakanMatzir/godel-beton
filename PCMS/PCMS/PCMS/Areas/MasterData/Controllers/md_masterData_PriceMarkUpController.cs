﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PCMS.Areas.MasterData.Models;
using PagedList;
using PCMS.Models;
using PCMS.Helper;

namespace PCMS.Areas.MasterData.Controllers
{
    [LogActionFilter]
    [Authorize]
    public class md_masterData_PriceMarkUpController : Controller
    {
        private MasterDataContext db = new MasterDataContext();

        public ActionResult Index(PaginationModel pg)
        {
            var model = db.Md_PriceMarkUp.ToList();

            if (pg != null)
            {
                foreach (PaginationFulltextFilter pgFF in pg.pagFulFilter)
                {
                    if (!string.IsNullOrWhiteSpace(pgFF.colVal))
                    {
                        pgFF.colVal = pgFF.colVal.ToLower();
                        switch (pgFF.colName)
                        {
                            case "Description":
                                model = model.Where(m => m.Description.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                            case "ShortDescription":
                                model = model.Where(m => m.ShortDescription.ToLower().Contains(pgFF.colVal)).ToList();
                                ViewBag.FilterCity = pgFF.colVal;
                                break;
                        }
                    }
                }

            }
            switch (pg.orderCol)
            {
                case "Id":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Id).ToList();
                    else
                        model = model.OrderByDescending(m => m.Id).ToList();
                    break;
                case "Description":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.Description).ToList();
                    else
                        model = model.OrderByDescending(m => m.Description).ToList();
                    break;
                case "ShortDescription":
                    if (pg.orderDir.Equals("desc"))
                        model = model.OrderBy(m => m.ShortDescription).ToList();
                    else
                        model = model.OrderByDescending(m => m.ShortDescription).ToList();
                    break;
            }

            return View(model.ToPagedList((pg.page ?? 1), pg.pageSize ?? Helper.Definitions.pageSize));
        }

        public ActionResult Create()
        {
            IList<PriceType> pTlist = new List<PriceType>();
            pTlist.Add(new PriceType{ Id=0,Description="Pauschal" });
            pTlist.Add(new PriceType { Id = 1, Description = "€/m³" });
            ViewBag.PriceType = new SelectList(pTlist, "Id", "Description");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(md_PriceMarkUp model)
        {
            if(ModelState.IsValid)
            {
                model.IsActive = true;
                model.IsDeleted = false;
                db.Md_PriceMarkUp.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            IList<PriceType> pTlist = new List<PriceType>();
            pTlist.Add(new PriceType { Id = 0, Description = "Pauschal" });
            pTlist.Add(new PriceType { Id = 1, Description = "€/m³" });
            ViewBag.PriceType = new SelectList(pTlist, "Id", "Description",model.PriceType);
            return View(model);
        }

        public ActionResult Edit(md_PriceMarkUp model, long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (db.Md_PriceMarkUp.Find(id) == null)
            {
                return HttpNotFound();
            }

            model = db.Md_PriceMarkUp.Find(id);
            IList<PriceType> pTlist = new List<PriceType>();
            pTlist.Add(new PriceType { Id = 0, Description = "Pauschal" });
            pTlist.Add(new PriceType { Id = 1, Description = "€/m³" });
            ViewBag.PriceType = new SelectList(pTlist, "Id", "Description", model.PriceType);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(md_PriceMarkUp model)
        {
            if(ModelState.IsValid)
            {
                
                model.IsActive = true;
                model.IsDeleted = false;

                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            IList<PriceType> pTlist = new List<PriceType>();
            pTlist.Add(new PriceType { Id = 0, Description = "Pauschal" });
            pTlist.Add(new PriceType { Id = 1, Description = "€/m³" });
            ViewBag.PriceType = new SelectList(pTlist, "Id", "Description", model.PriceType);
            return View(model);
        }
    }
    public class PriceType
    {
        public long Id { get; set; }
        public string Description { get; set; }
    }
}